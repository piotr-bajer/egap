import classnames from 'classnames';

const { __ } = wp.i18n;
const { Component, Fragment } = wp.element;
const { InspectorControls, MediaUpload, MediaUploadCheck, RichText } = wp.blockEditor;
const { PanelBody, Button, ResponsiveWrapper, Spinner } = wp.components;
const { compose } = wp.compose;
const { withSelect } = wp.data;

const ALLOWED_MEDIA_TYPES = [ 'image' ];

class HeroEdit extends Component {
	render() {
		const { attributes, setAttributes, bgImage, className } = this.props;
		const { img } = attributes;
		const instructions = <p>{ __( 'To edit the background image, you need permission to upload media.' ) }</p>;
		const bgSrc = bgImage && bgImage.source_url ? bgImage.source_url : '';
		const classes = classnames( className, 'c-hero o-wrapper-padding u-c-white' );

		const onUpdateImage = ( image ) => {
			setAttributes( {
				img: image.id,
			} );
		};

		const onRemoveImage = () => {
			setAttributes( {
				img: undefined,
			} );
		};

		return (
			<Fragment>
				<InspectorControls>
					<PanelBody
						title="Background settings"
						initialOpen={ true }
					>
						<div>
							<MediaUploadCheck fallback={ instructions }>
								<MediaUpload
									title="Background image"
									onSelect={ onUpdateImage }
									allowedTypes={ ALLOWED_MEDIA_TYPES }
									value={ img }
									render={ ( { open } ) => (
										<Button
											className={ ! img ? 'c-hero__button c-hero__button-toggle' : 'c-hero__button c-hero__button-preview' }
											onClick={ open }>
											{ ! img && 'Set background image' }
											{ !! img && ! bgImage && <Spinner /> }
											{ !! img && bgImage &&
												<ResponsiveWrapper
													naturalWidth={ bgImage.media_details.width }
													naturalHeight={ bgImage.media_details.height }
												>
													<img src={ bgImage.source_url } alt="Background image" />
												</ResponsiveWrapper>
											}
										</Button>
									) }
								/>
							</MediaUploadCheck>
							{ !! img && bgImage &&
							<MediaUploadCheck>
								<MediaUpload
									title="Background image"
									onSelect={ onUpdateImage }
									allowedTypes={ ALLOWED_MEDIA_TYPES }
									value={ img }
									render={ ( { open } ) => (
										<Button onClick={ open } isSecondary isLarge>
											Replace background image
										</Button>
									) }
								/>
							</MediaUploadCheck>
							}
							{ !! img &&
							<MediaUploadCheck>
								<Button onClick={ onRemoveImage } isLink isDestructive>
									Remove background image
								</Button>
							</MediaUploadCheck>
							}
						</div>
					</PanelBody>
				</InspectorControls>
				<div className={classes}>
					{ bgSrc && <img className="c-hero__image u-fit-cover" src={ bgSrc } alt="Background image" /> }
					<div className="c-hero__inner u-no-last-margin">
						<RichText
							tagName="h1"
							className="c-hero__title o-h3"
							value={ attributes.title }
							onChange={ ( title ) => setAttributes( { title } ) }
							placeholder={ 'Enter block title' }
							withoutInteractiveFormatting
							allowedFormats={ [] }
						/>
						<RichText
							tagName="div"
							className="c-hero__text"
							value={ attributes.text }
							onChange={ ( text ) => setAttributes( { text } ) }
							placeholder={ 'Enter block title' }
							allowedFormats={ [ 'core/bold' ] }
						/>
					</div>
				</div>
			</Fragment>
		);
	}
}

export default compose(
	withSelect( ( select, props ) => {
		const { getMedia } = select( 'core' );
		const { img } = props.attributes;

		return {
			bgImage: img ? getMedia( img ) : null,
		};
	} ),
)( HeroEdit );
