import './editor.scss';
import './style.scss';
import classnames from 'classnames';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'egap/block-egap-blocks', {
	title: __( 'egap-blocks - CGB Block' ),
	//icon: '',
	category: 'egap',
	keywords: [
		__( 'EGAP' ),
	],

	edit: ( { className } ) => {
		const classes = classnames( className, 'c-buttons' );
		return null;
	},

	save: ( ) => {
		return null;
	},
} );
