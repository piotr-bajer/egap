import './editor.scss';
import './style.scss';
import './store';
import classnames from 'classnames';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { withSelect } = wp.data;

registerBlockType( 'egap/breadcrumbs', {
	title: __( 'Breadcrumbs' ),
	//icon: '',
	category: 'egap',
	keywords: [
		__( 'EGAP' ),
	],

	edit: withSelect( ( select ) => {
		const postType = select( 'core/editor' ).getCurrentPostType();
		const breadcrumbs = [];
		let page = null;

		if ( postType === 'resource' ) {
			const resourcesPage = select( 'egap/breadcrumbs' ).getResourcesPage();
			if ( resourcesPage ) {
				page = select( 'core' ).getEntityRecord( 'postType', 'page', resourcesPage );

				if ( page ) {
					breadcrumbs.push( page.title.raw );
				}
			}
		} else if ( postType === 'page' ) {
			const postId = select( 'core/editor' ).getCurrentPostId();
			if ( postId ) {
				page = select( 'core' ).getEntityRecord( 'postType', 'page', postId );

				if ( page ) {
					while ( page && page.parent ) {
						page = select( 'core' ).getEntityRecord( 'postType', 'page', page.parent );

						if ( page ) {
							breadcrumbs.push( page.title.raw );
						}
					}
				}
			}
		}
		return { breadcrumbs: breadcrumbs.length > 0 ? breadcrumbs.reverse() : [ '(No breadcrumbs)' ] };
	} )( ( { className, breadcrumbs } ) => {
		const classes = classnames( className, 'c-breadcrumbs u-fsz-smallest' );
		return ( breadcrumbs &&
			<nav className={ classes }>
				{ breadcrumbs.map( ( title, index ) => {
					return <div key={ index } className={ 'c-breadcrumbs__item' }>{ title }</div>;
				} ) }
			</nav>
		);
	} ),

	save: ( ) => null,
} );
