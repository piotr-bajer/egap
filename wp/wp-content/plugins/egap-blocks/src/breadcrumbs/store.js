const { apiRequest } = wp;
const { registerStore, dispatch } = wp.data;

registerStore( 'egap/breadcrumbs', {
	reducer( state = {}, action ) {
		switch ( action.type ) {
			case 'EGAP_BREADCRUMBS':
				return action.data;
		}

		return state;
	},

	actions: {
		setResourcesPage( result ) {
			return {
				type: 'EGAP_BREADCRUMBS',
				data: result || {},
			};
		},
	},

	selectors: {
		getResourcesPage( result ) {
			return result.page_for_resources;
		},
	},

	resolvers: {
		async getResourcesPage() {
			const result = await apiRequest( { path: 'wp/v2/settings' } );
			dispatch( 'egap/breadcrumbs' ).setResourcesPage( result );
		},
	},
} );
