import './editor.scss';
import './style.scss';
import classnames from 'classnames';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { withSelect } = wp.data;
const { RichText } = wp.blockEditor;
const { dateI18n, __experimentalGetSettings } = wp.date;

registerBlockType( 'egap/page-date', {
	title: __( 'Page Date' ),
	//icon: '',
	category: 'egap',
	keywords: [
		__( 'EGAP' ),
	],
	attributes: {
		date: {
			type: 'string',
			default: undefined,
		},
	},

	edit: withSelect( ( select ) => {
		let date = select( 'core/editor' ).getEditedPostAttribute( 'date_gmt' );
		if ( ! date ) {
			const data = select( 'core/editor' ).getCurrentPost();
			date = data && data.date_gmt;
		}

		if ( date ) {
			date = dateI18n(
				// eslint-disable-next-line no-restricted-syntax
				__experimentalGetSettings().formats.date,
				date );
		}

		return { postDate: date };
	} )( ( { className, attributes, postDate, setAttributes } ) => {
		const { date } = attributes;
		const classes = classnames( className, 'c-page-date u-fw-700 u-c-heading u-uppercase' );

		return <RichText
			tagName="div"
			className={ classes }
			value={ date === undefined ? postDate : date }
			onChange={ ( text ) => setAttributes( { date: text } ) }
			placeholder={ 'Enter Post Date' }
			withoutInteractiveFormatting
			allowedFormats={ [] }
		/>;
	} ),

	save: ( ) => {
		return null;
	},
} );
