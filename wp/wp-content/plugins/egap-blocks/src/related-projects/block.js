//  Import CSS.
import './editor.scss';
import './style.scss';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Disabled } = wp.components;
const ServerSideRender = wp.serverSideRender;

registerBlockType( 'egap/related-projects', {
	title: __( 'Related Projects' ),
	icon: 'list-view',
	category: 'egap',
	keywords: [
		__( 'EGAP' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		anchor: {
			type: 'string',
		},
		title: {
			type: 'string',
		},
	},

	edit: () => {
		return <Disabled>
			<ServerSideRender
				block="egap/related-projects"
			/>
		</Disabled>;
	},

	save: () => {
		return null;
	},
} );
