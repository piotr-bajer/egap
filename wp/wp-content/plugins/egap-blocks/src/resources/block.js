//  Import CSS.
import './editor.scss';
import './style.scss';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Disabled } = wp.components;
const ServerSideRender = wp.serverSideRender;

registerBlockType( 'egap/resources', {
	title: __( 'Resources' ),
	icon: 'list-view',
	category: 'egap',
	keywords: [
		__( 'EGAP' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		align: {
			type: 'string',
			default: 'full',
		},
		anchor: {
			type: 'string',
		},
		title: {
			type: 'string',
		},
	},

	getEditWrapperProps( ) {
		return { 'data-align': 'full', align: 'full' };
	},

	edit: () => {
		return <Disabled>
			<ServerSideRender
				block="egap/resources"
			/>
		</Disabled>;
	},

	save: () => {
		return null;
	},
} );
