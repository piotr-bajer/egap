import './editor.scss';
import './style.scss';
import classnames from 'classnames';

const { InnerBlocks } = wp.blockEditor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'egap/resource-tiles', {
	title: __( 'Resource Tiles' ),
	icon: 'grid-view',
	category: 'egap',
	keywords: [
		__( 'EGAP' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		anchor: {
			type: 'string',
		},
	},

	edit: ( { className } ) => {
		const classes = classnames( className, 'c-resource-tiles' );
		return (
			<div className={ classes }>
				<InnerBlocks
					className="c-resource-tiles__items c-research-tiles"
					allowedBlocks={ [ 'egap/resource-tiles-item' ] }
					template={ [ [ 'egap/resource-tiles-item' ] ] }
				/>
			</div>
		);
	},

	save: ( ) => {
		return <InnerBlocks.Content />;
	},
} );
