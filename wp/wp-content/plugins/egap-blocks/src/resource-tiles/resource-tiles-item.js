import classnames from 'classnames';
import URLPicker from '../common/url-picker';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText, InspectorControls } = wp.blockEditor;
const { Fragment } = wp.element;
const { PanelBody, ColorPalette } = wp.components;
const { withSelect } = wp.data;

registerBlockType( 'egap/resource-tiles-item', {
	title: __( 'Tile' ),
	parent: [ 'egap/resource-tiles' ],
	category: 'egap',
	supports: {
		reusable: false,
		html: false,
	},
	keywords: [
		__( 'EGAP' ),
	],
	attributes: {
		title: {
			type: 'string',
		},
		text: {
			type: 'string',
		},
		strapline: {
			type: 'string',
		},
		url: {
			type: 'string',
		},
		target: {
			type: 'string',
		},
		rel: {
			type: 'string',
		},
		color: {
			type: 'string',
			default: '',
		},
	},

	edit: withSelect( select => {
		const colors = select( 'core/editor' ).getEditorSettings().colors;
		return { colors: colors ? colors : [] };
	} )( ( { className, attributes, setAttributes, isSelected, colors } ) => {
		const classes = classnames( className, 'c-research-tile u-no-last-margin' );
		const { title, text, strapline, url, target, rel, color } = attributes;
		return (
			<Fragment>
				<InspectorControls>
					<PanelBody
						title="Line color"
						initialOpen={ true }
					>
						<ColorPalette
							colors={ colors }
							value={ color }
							onChange={ ( newColor ) => setAttributes( { color: newColor } ) }
						/>
					</PanelBody>
				</InspectorControls>
				<div className={ classes }>
					<hr className="c-research-tile__bar" style={ color ? { color } : {} } />
					<div className="c-resource-tiles__content">
						<RichText
							className="c-research-tile__type u-fw-700 u-c-heading"
							value={ strapline }
							onChange={ ( content ) => setAttributes( { strapline: content } ) }
							placeholder={ 'Enter Tile Strapline' }
							withoutInteractiveFormatting
							allowedFormats={ [] }
						/>
						<RichText
							className="c-research-tile__title o-h5 u-fw-400"
							value={ title }
							onChange={ ( content ) => setAttributes( { title: content } ) }
							placeholder={ 'Enter Tile Title' }
							withoutInteractiveFormatting
							allowedFormats={ [] }
						/>
						<div className="c-research-tile__footer u-fsz-smallest u-c-gray-dark">
							<RichText
								className="c-research-tile__footer-item"
								value={ text }
								onChange={ ( content ) => setAttributes( { text: content } ) }
								placeholder={ 'Enter Tile Text' }
								withoutInteractiveFormatting
								allowedFormats={ [] }
							/>
						</div>
					</div>
					<URLPicker
						url={ url }
						setAttributes={ setAttributes }
						isSelected={ isSelected }
						opensInNewTab={ target === '_blank' }
						rel={ rel }
					/>
				</div>
			</Fragment>
		);
	} ),

	save: () => null,
} );
