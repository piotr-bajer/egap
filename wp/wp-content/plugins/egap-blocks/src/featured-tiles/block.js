import './editor.scss';
import './style.scss';
import classnames from 'classnames';

const { InnerBlocks, RichText } = wp.blockEditor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'egap/featured-tiles', {
	title: __( 'Featured Tiles' ),
	icon: 'grid-view',
	category: 'egap',
	keywords: [
		__( 'EGAP' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		align: {
			type: 'string',
			default: 'full',
		},
		title: {
			type: 'string',
		},
		anchor: {
			type: 'string',
		},
	},

	getEditWrapperProps( ) {
		return { 'data-align': 'full', align: 'full' };
	},

	edit: ( { className, attributes, setAttributes } ) => {
		const classes = classnames( className, 'c-featured-tiles o-wrapper-padding' );
		const { title } = attributes;

		return (
			<div className={ classes }>
				<RichText
					className="c-featured-tiles__title o-h3 u-fw-600 u-c-blue-darker"
					tagName="h3"
					value={ title }
					onChange={ ( content ) => setAttributes( { title: content } ) }
					placeholder={ 'Enter Block Title' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<InnerBlocks
					className="c-featured-tiles__items"
					allowedBlocks={ [ 'egap/featured-tiles-item' ] }
					template={ [ [ 'egap/featured-tiles-item' ] ] }
				/>
			</div>
		);
	},

	save: ( ) => {
		return <InnerBlocks.Content />;
	},
} );
