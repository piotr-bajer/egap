import './editor.scss';
import './style.scss';
import classnames from 'classnames';

const { InnerBlocks } = wp.blockEditor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { dispatch, select } = wp.data;

registerBlockType( 'egap/image-tiles', {
	title: __( 'Image Tiles' ),
	icon: 'grid-view',
	category: 'egap',
	keywords: [
		__( 'EGAP' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		anchor: {
			type: 'string',
		},
	},
	styles: [
		{
			name: 'default',
			label: __( 'Two Columns Layout' ),
			isDefault: true,
		},
		{
			name: 'vertical',
			label: __( 'One Column Layout' ),
		},
	],

	edit: ( { className, clientId } ) => {
		const classes = classnames( className, 'c-image-tiles' );

		const type = className && className.indexOf( 'vertical' ) > -1 ? 'vertical' : 'horizontal';

		select( 'core/editor' ).getBlocksByClientId( clientId )[ 0 ].innerBlocks.forEach( ( block ) => {
			dispatch( 'core/editor' ).updateBlockAttributes( block.clientId, { type } );
		} );

		return (
			<div className={ classes }>
				<InnerBlocks
					className="c-image-tiles__items"
					allowedBlocks={ [ 'egap/image-tiles-item' ] }
					template={ [ [ 'egap/image-tiles-item' ] ] }
				/>
			</div>
		);
	},

	save: ( ) => {
		return <InnerBlocks.Content />;
	},
} );
