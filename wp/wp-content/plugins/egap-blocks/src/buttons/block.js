import './editor.scss';
import './style.scss';
import classnames from 'classnames';

const { InnerBlocks } = wp.blockEditor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'egap/buttons', {
	title: __( 'Buttons' ),
	icon: 'button',
	category: 'egap',
	keywords: [
		__( 'EGAP' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		anchor: {
			type: 'string',
		},
	},
	styles: [
		{
			name: 'default',
			label: __( 'Horizontal Layout' ),
			isDefault: true,
		},
		{
			name: 'vertical',
			label: __( 'Vertical Layout' ),
		},
	],

	edit: ( { className } ) => {
		const classes = classnames( className, 'c-buttons' );
		return (
			<InnerBlocks
				className={ classes }
				allowedBlocks={ [ 'egap/buttons-item' ] }
				template={ [ [ 'egap/buttons-item' ] ] }
			/>
		);
	},

	save: ( ) => <InnerBlocks.Content />,
} );
