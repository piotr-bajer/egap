import classnames from 'classnames';
import URLPicker from '../common/url-picker';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText } = wp.blockEditor;
const { Fragment } = wp.element;

registerBlockType( 'egap/buttons-item', {
	title: __( 'Button' ),
	parent: [ 'egap/buttons' ],
	category: 'egap',
	supports: {
		reusable: false,
		html: false,
	},
	keywords: [
		__( 'EGAP' ),
	],
	attributes: {
		text: {
			type: 'string',
		},
		url: {
			type: 'string',
		},
		target: {
			type: 'string',
		},
		rel: {
			type: 'string',
		},
	},
	styles: [
		{
			name: 'default',
			label: __( 'Default' ),
			isDefault: true,
		},
		{
			name: 'outline',
			label: __( 'Outline' ),
		},
		{
			name: 'outline-white',
			label: __( 'Outline White' ),
		},
	],

	edit: ( { className, attributes, setAttributes, isSelected } ) => {
		const classes = classnames( className, 'c-buttons__item o-btn' );
		const { text, url, target, rel } = attributes;

		return (
			<Fragment>
				<RichText
					className={ classes }
					value={ text }
					onChange={ ( content ) => setAttributes( { text: content } ) }
					placeholder={ 'Add text' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<URLPicker
					url={ url }
					setAttributes={ setAttributes }
					isSelected={ isSelected }
					opensInNewTab={ target === '_blank' }
					rel={ rel }
				/>
			</Fragment>
		);
	},

	save: () => null,
} );
