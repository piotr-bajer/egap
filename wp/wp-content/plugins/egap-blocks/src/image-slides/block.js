import './editor.scss';
import './style.scss';
import classnames from 'classnames';

const { RichText, MediaUploadCheck, MediaUpload } = wp.blockEditor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Button, Icon } = wp.components;
const { Fragment } = wp.element;
const { withSelect } = wp.data;

const instructions = <p>{ __( 'To edit the image, you need permission to upload media.' ) }</p>;

registerBlockType( 'egap/image-slides', {
	title: __( 'Image Slides' ),
	icon: 'image',
	category: 'egap',
	keywords: [
		__( 'EGAP' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		anchor: {
			type: 'string',
		},
		slides: {
			type: 'array',
			default: [
				{
					title: 'Slide title',
					img: undefined,
				},
			],
		},
		currentSlide: {
			type: 'number',
			default: 0,
		},
	},

	edit: withSelect( ( select, props ) => {
		const { getMedia } = select( 'core' );
		const slideData = props.attributes.slides[ props.attributes.currentSlide ];
		const img = slideData.img === undefined ? null : slideData.img;

		return {
			bgImage: img ? getMedia( img ) : null,
		};
	} )( ( { attributes, isSelected, setAttributes, className, bgImage } ) => {
		const classes = classnames( className, 'c-image-slides' );
		const { slides, currentSlide } = attributes;
		const bgSrc = bgImage && bgImage.source_url ? bgImage.source_url : '';

		const onAddSlide = () => {
			const newSlides = JSON.parse( JSON.stringify( slides ) );
			newSlides.push( {
				title: 'Slide title',
				img: undefined,
			} );
			setAttributes( { slides: newSlides, currentSlide: newSlides.length - 1 } );
		};

		const onRemoveSlide = () => {
			const newSlides = JSON.parse( JSON.stringify( slides ) );
			newSlides.splice( currentSlide, 1 );
			setAttributes( { slides: newSlides, currentSlide: newSlides.length - 1 } );
		};

		const setSlide = ( index ) => {
			setAttributes( { currentSlide: index } );
		};

		const slideData = attributes.slides[ attributes.currentSlide ];

		const onTitleChange = ( content, index ) => {
			const newSlides = JSON.parse( JSON.stringify( slides ) );
			newSlides[ index ].title = content;
			setAttributes( { slides: newSlides } );
		};

		const onImgChange = ( image ) => {
			const newSlides = JSON.parse( JSON.stringify( slides ) );
			newSlides[ currentSlide ].img = image.id;
			setAttributes( { slides: newSlides } );
		};
		const nav = slides.map( ( data, index ) => (
			// eslint-disable-next-line jsx-a11y/click-events-have-key-events
			<li className="o-filter-header__item" key={ index } onClick={ () => setSlide( index ) }>
				<RichText
					tagName="div"
					className={ ( index === currentSlide ? 'is-active ' : '' ) + 'o-filter-header__button' }
					value={ data.title }
					onChange={ ( title ) => onTitleChange( title, index ) }
					placeholder={ 'Enter slide title' }
					withoutInteractiveFormatting
					allowedFormats={ [ 'core/bold' ] }
				/>
			</li>
		) );

		return (
			<div className={ classes }>
				<ul className="c-image-slides__header o-filter-header o-list-bare">
					{ nav }
				</ul>
				<div className="c-image-slides__items">
					<MediaUploadCheck fallback={ instructions }>
						<MediaUpload
							onSelect={ onImgChange }
							allowedTypes={ [ 'image' ] }
							value={ slideData.img }
							render={ ( { open } ) => <div className={ 'c-image-slides__item is-active' }>
								{ slideData.img ?
									<Fragment>
										<img className="c-image-slides__image" src={ bgSrc } alt="" />
										{ isSelected && <Fragment>
											<Button isSecondary onClick={ () => {
												onImgChange( undefined );
											} }>
												Remove Image
											</Button></Fragment> }
									</Fragment> :
									<Button isSecondary onClick={ open }>
										Add Image
									</Button> }
							</div> }
						/>
					</MediaUploadCheck>
				</div>
				{ isSelected && <div style={ { display: 'flex' } }>
					<Button
						className={ 'block-editor-button-block-appender' }
						onClick={ onAddSlide }
					>
						Add slide
						<Icon icon="plus" />
					</Button>
					<Button
						className={ 'block-editor-button-block-appender' }
						onClick={ onRemoveSlide }
					>
						Remove slide
						<Icon icon="minus" />
					</Button>
				</div> }
			</div>
		);
	} ),

	save: ( ) => null,
} );
