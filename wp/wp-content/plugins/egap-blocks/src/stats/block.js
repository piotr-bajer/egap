import './editor.scss';
import './style.scss';
import classnames from 'classnames';

const { InnerBlocks } = wp.blockEditor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'egap/stats', {
	title: __( 'Stats' ),
	icon: 'grid-view',
	category: 'egap',
	keywords: [
		__( 'EGAP' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		anchor: {
			type: 'string',
		},
	},

	edit: ( { className } ) => {
		const classes = classnames( className, 'c-stats o-generic-links' );
		return (
			<InnerBlocks
				className={ classes }
				allowedBlocks={ [ 'egap/stats-item' ] }
				template={ [ [ 'egap/stats-item' ], [ 'egap/stats-item' ], [ 'egap/stats-item' ], [ 'egap/stats-item' ] ] }
			/>
		);
	},

	save: ( { className } ) => {
		const classes = classnames( className, 'c-stats o-generic-links' );
		return <section className={ classes }>
			<InnerBlocks.Content />
		</section>;
	},
} );
