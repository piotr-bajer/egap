import classnames from 'classnames';
import URLPicker from '../common/url-picker';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText } = wp.blockEditor;

registerBlockType( 'egap/stats-item', {
	title: __( 'Stat' ),
	parent: [ 'egap/stats', 'egap/image-stats' ],
	category: 'egap',
	supports: {
		reusable: false,
		html: false,
	},
	keywords: [
		__( 'EGAP' ),
	],
	attributes: {
		stat: {
			type: 'string',
		},
		description: {
			type: 'string',
		},
		url: {
			type: 'string',
		},
		target: {
			type: 'string',
		},
		rel: {
			type: 'string',
		},
	},

	edit: ( { className, attributes, setAttributes, isSelected } ) => {
		const classes = classnames( className, 'c-stats__item' );
		const { description, stat, url, target, rel } = attributes;

		return (
			<div className={ classes }>
				<RichText
					className="c-stats__stat"
					value={ stat }
					onChange={ ( text ) => setAttributes( { stat: text } ) }
					placeholder={ 'Enter Stat' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<RichText
					className="c-stats__description u-fw-700"
					value={ description }
					onChange={ ( text ) => setAttributes( { description: text } ) }
					placeholder={ 'Enter Stat Label' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<URLPicker
					url={ url }
					setAttributes={ setAttributes }
					isSelected={ isSelected }
					opensInNewTab={ target === '_blank' }
					rel={ rel }
				/>
			</div>
		);
	},

	save: ( { className, attributes } ) => {
		const classes = classnames( className, 'c-stats__item u-c-heading' );
		const { description, stat, url } = attributes;

		return <div className={ classes }>
			<div className="c-stats__stat">
				{ stat }
			</div>
			{ url ? <a className="c-stats__description u-fw-700" href={ url }>{ description }</a> : <div className="c-stats__description u-fw-700">
				{ description }
			</div> }
		</div>;
	},
} );
