import './editor.scss';
import './style.scss';
import classnames from 'classnames';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText, InnerBlocks } = wp.blockEditor;

registerBlockType( 'egap/cover', {
	title: __( 'Cover Section' ),
	icon: 'format-image',
	category: 'egap',
	keywords: [
		__( 'EGAP' ),
	],
	attributes: {
		align: {
			type: 'string',
			default: 'full',
		},
		title: {
			type: 'string',
		},
		text: {
			type: 'string',
		},
	},
	supports: {
		html: false,
		anchor: true,
	},

	getEditWrapperProps( ) {
		return { 'data-align': 'full', align: 'full' };
	},

	edit: ( { attributes, setAttributes, className } ) => {
		const classes = classnames( className, 'c-cover o-wrapper-padding u-c-white u-text-center' );
		return (
			<div className={ classes }>
				<RichText
					tagName="h2"
					className="c-cover__title o-h4"
					value={ attributes.title }
					onChange={ ( title ) => setAttributes( { title } ) }
					placeholder={ 'Enter block title' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<RichText
					tagName="div"
					className="c-cover__text"
					value={ attributes.text }
					onChange={ ( text ) => setAttributes( { text } ) }
					placeholder={ 'Enter block text' }
					allowedFormats={ [ 'core/bold' ] }
				/>
				<InnerBlocks
					className="c-buttons u-mb-0"
					templateLock={ false }
					allowedBlocks={ [ 'egap/buttons-item' ] }
					template={ [
						[ 'egap/buttons-item' ],
						[ 'egap/buttons-item', { className: 'is-style-outline-white' } ],
					] }
				/>
			</div>
		);
	},

	save: () => {
		return <InnerBlocks.Content />;
	},
} );
