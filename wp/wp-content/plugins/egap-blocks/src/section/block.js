/**
 * BLOCK: partnership-blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';
import classnames from 'classnames';

const { __ } = wp.i18n;
const { InnerBlocks, InspectorControls } = wp.blockEditor;
const { registerBlockType } = wp.blocks;
const { PanelBody, ColorPalette } = wp.components;
const { Fragment } = wp.element;

const colors = [
	{ name: 'white', color: '#fff' },
	{ name: 'dark', color: '#1e262f' },
];

registerBlockType( 'egap/section', {
	title: __( 'Section' ),
	icon: 'section',
	category: 'egap',
	keywords: [
		__( 'EGAP' ),
	],
	attributes: {
		align: {
			type: 'string',
			default: 'full',
		},
		backgroundColor: {
			type: 'string',
			default: '#fff',
		},
		anchor: {
			type: 'string',
		},
	},
	supports: {
		html: false,
		anchor: true,
	},

	getEditWrapperProps( ) {
		return { 'data-align': 'full', align: 'full' };
	},

	edit: ( { attributes, setAttributes, className } ) => {
		const { backgroundColor } = attributes;
		const classes = classnames( className, 'c-section o-wrapper-padding' );
		return (
			<Fragment>
				<InspectorControls>
					<PanelBody
						title="Background color"
						initialOpen={ true }
					>
						<ColorPalette
							colors={ colors }
							value={ backgroundColor }
							onChange={ ( color ) => setAttributes( { backgroundColor: color } ) }
						/>
					</PanelBody>
				</InspectorControls>
				<section className={ classes } style={ { backgroundColor } }>
					<InnerBlocks
						template={ [ [ 'egap/section-columns' ] ] }
						allowedBlocks={ [ 'egap/section-columns' ] }
					/>
				</section>
			</Fragment>
		);
	},

	save: () => {
		return <InnerBlocks.Content />;
	},
} );
