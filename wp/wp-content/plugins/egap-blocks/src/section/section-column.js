/**
 * BLOCK: partnership-blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

import classnames from 'classnames';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { InnerBlocks } = wp.blockEditor;

registerBlockType( 'egap/section-column', {
	title: __( 'Single Column Row' ),
	parent: [ 'egap/section' ],
	supports: {
		reusable: false,
		html: false,
	},
	category: 'egap',

	edit: ( { className } ) => {
		const classes = classnames( className, 'c-section__column' );
		return (
			<InnerBlocks
				template={ [ [ 'core/paragraph' ] ] }
				className={ classes }
			/>
		);
	},

	save: () => {
		return <InnerBlocks.Content />;
	},
} );
