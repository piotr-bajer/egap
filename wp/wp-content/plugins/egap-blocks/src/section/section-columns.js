/**
 * BLOCK: partnership-blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

import classnames from 'classnames';

const { __ } = wp.i18n;
const { Fragment } = wp.element;
const { registerBlockType } = wp.blocks;
const { InnerBlocks, InspectorControls } = wp.blockEditor;
const { PanelBody, ToggleControl } = wp.components;

registerBlockType( 'egap/section-columns', {
	title: __( 'Two Columns Row' ),
	parent: [ 'egap/section' ],
	supports: {
		reusable: false,
		html: false,
	},
	category: 'egap',
	attributes: {
		reverseOrder: {
			type: 'boolean',
			default: false,
		},
	},

	edit: ( { attributes, setAttributes, className } ) => {
		const { reverseOrder } = attributes;
		const classes = classnames( className, 'c-section__columns', { 'c-section__columns--reverse': reverseOrder } );
		return (
			<Fragment>
				<InspectorControls>
					<PanelBody initialOpen={ true }>
						<ToggleControl
							label={ __( 'Reverse columns order on collapse' ) }
							help={
								reverseOrder ?
									'Columns will be reversed' :
									'Columns will be collapsed normally'
							}
							checked={ reverseOrder }
							onChange={ () =>
								setAttributes( { reverseOrder: ! reverseOrder } )
							}
						/>
					</PanelBody>
				</InspectorControls>
				<InnerBlocks
					template={ [ [ 'core/column', { }, [ [ 'core/paragraph' ] ] ], [ 'core/column', { }, [ [ 'core/paragraph' ] ] ] ] }
					templateLock="insert"
					allowedBlocks={ [ 'core/column' ] }
					className={ classes }
				/>
			</Fragment>
		);
	},

	save: () => {
		return <InnerBlocks.Content />;
	},
} );
