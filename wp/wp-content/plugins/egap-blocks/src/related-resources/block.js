//  Import CSS.
import './editor.scss';
import './style.scss';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Fragment } = wp.element;
const { InspectorControls } = wp.blockEditor;
const { SelectControl, PanelBody, ToggleControl, TextControl, Disabled } = wp.components;
const { withSelect } = wp.data;
const ServerSideRender = wp.serverSideRender;

registerBlockType( 'egap/related-resources', {
	title: __( 'Related Resources' ),
	icon: 'list-view',
	category: 'egap',
	keywords: [
		__( 'EGAP' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		anchor: {
			type: 'string',
		},
		title: {
			type: 'string',
		},
		types: {
			type: 'array',
			default: [],
		},
		filterBy: {
			type: 'string',
			default: '',
		},
		limit: {
			type: 'number',
			default: 0,
		},
		limitResults: {
			type: 'boolean',
			default: false,
		},
		showRelated: {
			type: 'boolean',
			default: true,
		},
		viewAll: {
			type: 'boolean',
			default: false,
		},
		viewAllText: {
			type: 'string',
			default: 'View More',
		},
	},

	edit: withSelect( ( select ) => {
		const data = select( 'core' ).getEntityRecords( 'taxonomy', 'resource-type', { per_page: -1 } );
		const allTypes = data ? data.map( ( type ) => {
			return { label: type.name, value: type.id };
		} ) : [];

		return { allTypes: [ { label: 'All Types', value: 0 }, ...allTypes ] };
	} )( ( { allTypes, setAttributes, attributes } ) => {
		const { types, filterBy, viewAll, showRelated, viewAllText, limitResults, limit } = attributes;

		const onChange = value => {
			setAttributes( { types: value } );
		};

		return <Fragment>
			<InspectorControls>
				<PanelBody title={ __( 'Filtering Options' ) }>
					<SelectControl
						multiple
						label="Select resource types to narrow selection"
						value={ types }
						options={ allTypes }
						onChange={ onChange }
					/>
					<ToggleControl
						label={ __( 'Only display resources related to this page?' ) }
						onChange={ ( value ) => {
							setAttributes( { showRelated: value } );
						} }
						checked={ showRelated }
					/>
					<ToggleControl
						label={ __( 'Group items by Country?' ) }
						onChange={ ( value ) => {
							setAttributes( { filterBy: value ? 'country' : '' } );
						} }
						checked={ filterBy === 'country' }
					/>
					<ToggleControl
						label={ __( 'Display "View All Button"?' ) }
						onChange={ ( value ) => {
							setAttributes( { viewAll: value } );
						} }
						checked={ viewAll }
					/>
					{ viewAll && <TextControl label={ __( '"View All Button" label' ) }
						value={ viewAllText }
						onChange={ ( value ) => setAttributes( { viewAllText: value } ) }
					/> }
					<ToggleControl
						label={ __( 'Limit results?' ) }
						onChange={ ( value ) => {
							setAttributes( { limitResults: value } );
						} }
						checked={ limitResults }
					/>
					{ limitResults && <TextControl label={ __( 'Results limit' ) }
						type="number"
						value={ limit }
						onChange={ ( value ) => setAttributes( { limit: parseInt( value, 10 ) } ) }
					/> }
				</PanelBody>
			</InspectorControls>
			<Disabled>
				<ServerSideRender
					attributes={ attributes }
					block="egap/related-resources"
				/>
			</Disabled>
		</Fragment>
		;
	} ),

	save: () => {
		return null;
	},
} );
