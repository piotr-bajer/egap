/**
 * Gutenberg Blocks
 *
 * All blocks related JavaScript files should be imported here.
 * You can create a new block folder in this dir and include code
 * for that block here as well.
 *
 * All blocks should be included here since this is the file that
 * Webpack is compiling as the input file.
 */

//import './block/block';
import './section/section-column';
import './section/section-columns';
import './section/block';
import './hero/block';
import './stats/stats-item';
import './stats/block';
import './buttons/block';
import './buttons/buttons-item';
import './image-stats/block';
import './image-tiles/image-tiles-item';
import './image-tiles/block';
import './cover/block';
import './icon-tiles/icon-tiles-item';
import './icon-tiles/block';
import './breadcrumbs/block';
import './page-title/block';
import './page-date/block';
import './featured-tiles/featured-tiles-item';
import './featured-tiles/block';
import './members/block';
import './resources/block';
import './related-projects/block';
import './submenu/submenu-item';
import './submenu/block';
import './related-resources/block';
import './image-slides/block';
import './resource-tiles/resource-tiles-item';
import './resource-tiles/block';
import './design-registry/block';
import './staff/block';
