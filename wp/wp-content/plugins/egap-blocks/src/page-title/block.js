import './editor.scss';
import './style.scss';
import classnames from 'classnames';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { withSelect } = wp.data;
const { RichText } = wp.blockEditor;

registerBlockType( 'egap/page-title', {
	title: __( 'Page Title' ),
	//icon: '',
	category: 'egap',
	keywords: [
		__( 'EGAP' ),
	],
	attributes: {
		title: {
			type: 'string',
			default: undefined,
		},
	},
	styles: [
		{
			name: 'default',
			label: __( 'Big' ),
			isDefault: true,
		},
		{
			name: 'smaller',
			label: __( 'Smaller' ),
		},
	],

	edit: withSelect( ( select ) => {
		let title = select( 'core/editor' ).getEditedPostAttribute( 'title' );
		if ( ! title ) {
			const data = select( 'core/editor' ).getCurrentPost();
			title = data && data.title;
		}

		return { postTitle: title };
	} )( ( { className, attributes, postTitle, setAttributes } ) => {
		const { title } = attributes;
		const additionalClass = className.indexOf( 'smaller' ) < 0 ? 'o-h1' : 'o-h2';
		const classes = classnames( className, 'c-page-title', additionalClass );

		return <RichText
			tagName="h1"
			className={ classes }
			value={ title === undefined ? postTitle : title }
			onChange={ ( text ) => setAttributes( { title: text } ) }
			placeholder={ 'Enter Post Title' }
			withoutInteractiveFormatting
			allowedFormats={ [] }
		/>;
	} ),

	save: ( ) => {
		return null;
	},
} );
