/**
 * BLOCK: partnership-blocks
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'egap/anchor', {
	title: __( 'Anchor' ),
	icon: 'admin-links',
	category: 'egap',
	keywords: [
		__( 'EGAP' ),
	],
	supports: {
		anchor: true,
	},
	attributes: {
		anchor: {
			type: 'string',
		},
	},

	edit: ( { className, attributes } ) => {
		return (
			<div className={ className }>
				<div>
					<span className="dashicons dashicons-admin-links" />
					{ attributes.anchor && <span className="wp-block-egap-anchor__label">{ attributes.anchor }</span> }
				</div>
			</div>
		);
	},

	save: ( ) => {
		return null;
	},
} );
