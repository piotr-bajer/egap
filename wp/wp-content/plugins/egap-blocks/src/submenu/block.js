import './editor.scss';
import './style.scss';
import classnames from 'classnames';

const { InnerBlocks } = wp.blockEditor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'egap/submenu', {
	title: __( 'Submenu' ),
	icon: 'menu',
	category: 'egap',
	keywords: [
		__( 'EGAP' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		anchor: {
			type: 'string',
		},
	},
	styles: [
		{
			name: 'default',
			label: __( 'Horizontal Layout' ),
			isDefault: true,
		},
		{
			name: 'vertical',
			label: __( 'Vertical Layout' ),
		},
	],

	edit: ( { className } ) => {
		const classes = classnames( className, 'c-submenu' );
		return (
			<div className={ classes }>
				<InnerBlocks
					className="c-submenu__items"
					allowedBlocks={ [ 'egap/submenu-item' ] }
					template={ [ [ 'egap/submenu-item' ] ] }
				/>
			</div>
		);
	},

	save: ( ) => <InnerBlocks.Content />,
} );
