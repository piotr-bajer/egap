//  Import CSS.
import './editor.scss';
import './style.scss';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Disabled } = wp.components;
const ServerSideRender = wp.serverSideRender;

registerBlockType( 'egap/members', {
	title: __( 'Members' ),
	icon: 'list-view',
	category: 'egap',
	keywords: [
		__( 'EGAP' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		align: {
			type: 'string',
			default: 'full',
		},
		anchor: {
			type: 'string',
		},
		title: {
			type: 'string',
		},
	},

	getEditWrapperProps( ) {
		return { 'data-align': 'full', align: 'full' };
	},

	edit: () => {
		return <Disabled>
			<ServerSideRender
				block="egap/members"
			/>
		</Disabled>;
	},

	save: () => {
		return null;
	},
} );
