import classnames from 'classnames';
import URLPicker from '../common/url-picker';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText, MediaUpload, MediaUploadCheck } = wp.blockEditor;
const { Fragment } = wp.element;
const { Button } = wp.components;
const { withSelect } = wp.data;

const instructions = <p>{ __( 'To edit the image, you need permission to upload media.' ) }</p>;
const ALLOWED_MEDIA_TYPES = [ 'image' ];

registerBlockType( 'egap/icon-tiles-item', {
	title: __( 'Tile' ),
	parent: [ 'egap/icon-tiles' ],
	category: 'egap',
	supports: {
		reusable: false,
		html: false,
	},
	keywords: [
		__( 'EGAP' ),
	],
	attributes: {
		title: {
			type: 'string',
		},
		text: {
			type: 'string',
		},
		buttonText: {
			type: 'string',
		},
		url: {
			type: 'string',
		},
		target: {
			type: 'string',
		},
		rel: {
			type: 'string',
		},
		img: {
			type: 'number',
		},
	},

	edit: withSelect( ( select, props ) => {
		const { getMedia } = select( 'core' );
		const { img } = props.attributes;

		return {
			bgImage: img ? getMedia( img ) : null,
		};
	} )( ( { className, attributes, setAttributes, isSelected, bgImage } ) => {
		const classes = classnames( className, 'c-icon-tiles__item' );
		const { title, text, buttonText, url, target, rel, img } = attributes;
		const bgSrc = bgImage && bgImage.source_url ? bgImage.source_url : '';

		return (
			<div className={ classes }>
				{ bgSrc && <img className="c-icon-tiles__image u-fit-contain" src={ bgSrc } alt="" /> }
				<MediaUploadCheck fallback={ instructions }>
					<MediaUpload
						onSelect={ ( image ) => {
							setAttributes( {
								img: image.id,
							} );
						} }
						allowedTypes={ ALLOWED_MEDIA_TYPES }
						value={ img }
						render={ ( { open } ) => img ?
							<Fragment>
								{ isSelected && <Fragment>
									<Button isSecondary onClick={ () => {
										setAttributes( {
											img: undefined,
										} );
									} }>
										Remove Image
									</Button></Fragment> }
							</Fragment> :
							( <Button isSecondary onClick={ open }>
								Add Image
							</Button> ) }
					/>
				</MediaUploadCheck>
				<RichText
					className="c-icon-tiles__title u-fw-700"
					value={ title }
					onChange={ ( content ) => setAttributes( { title: content } ) }
					placeholder={ 'Enter Tile Title' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<RichText
					className="c-icon-tiles__text"
					value={ text }
					onChange={ ( content ) => setAttributes( { text: content } ) }
					placeholder={ 'Enter Tile Text' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<RichText
					className="c-icon-tiles__button o-btn o-btn--outline"
					value={ buttonText }
					onChange={ ( content ) => setAttributes( { buttonText: content } ) }
					placeholder={ 'Enter Button Text' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<URLPicker
					url={ url }
					setAttributes={ setAttributes }
					isSelected={ isSelected }
					opensInNewTab={ target === '_blank' }
					rel={ rel }
				/>
			</div>
		);
	} ),

	save: () => null,
} );
