import './editor.scss';
import './style.scss';
import classnames from 'classnames';

const { InnerBlocks } = wp.blockEditor;
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

registerBlockType( 'egap/icon-tiles', {
	title: __( 'Icon Tiles' ),
	icon: 'grid-view',
	category: 'egap',
	keywords: [
		__( 'EGAP' ),
	],
	supports: {
		html: false,
		anchor: true,
	},
	attributes: {
		anchor: {
			type: 'string',
		},
	},

	edit: ( { className } ) => {
		const classes = classnames( className, 'c-icon-tiles' );
		return (
			<div className={ classes }>
				<InnerBlocks
					className="c-icon-tiles__items u-fsz-small"
					allowedBlocks={ [ 'egap/icon-tiles-item' ] }
					template={ [ [ 'egap/icon-tiles-item' ] ] }
				/>
			</div>
		);
	},

	save: ( ) => {
		return <InnerBlocks.Content />;
	},
} );
