import './editor.scss';
import './style.scss';
import edit from './edit';

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { InnerBlocks } = wp.blockEditor;

registerBlockType( 'egap/image-stats', {
	title: __( 'Image & Stats' ),
	icon: 'format-image',
	category: 'egap',
	keywords: [
		__( 'EGAP' ),
	],
	attributes: {
		align: {
			type: 'string',
			default: 'full',
		},
		title: {
			type: 'string',
		},
		caption: {
			type: 'string',
		},
		img: {
			type: 'number',
		},
		anchor: {
			type: 'string',
		},
	},
	supports: {
		html: false,
		anchor: true,
	},

	getEditWrapperProps( ) {
		return { 'data-align': 'full', align: 'full' };
	},

	edit: edit,

	save: ( ) => {
		return <section className="c-stats c-stats--matrix o-generic-links">
			<InnerBlocks.Content />
		</section>;
	},
} );
