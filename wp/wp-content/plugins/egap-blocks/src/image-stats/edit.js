const { __ } = wp.i18n;
const { Component, Fragment } = wp.element;
const { MediaUpload, MediaUploadCheck, RichText, InnerBlocks } = wp.blockEditor;
const { Button } = wp.components;
const { compose } = wp.compose;
const { withSelect } = wp.data;

const ALLOWED_MEDIA_TYPES = [ 'image' ];

class ImageStatsEdit extends Component {
	render() {
		const { attributes, setAttributes, bgImage, isSelected } = this.props;
		const { img } = attributes;
		const instructions = <p>{ __( 'To edit the image, you need permission to upload media.' ) }</p>;

		const bgSrc = bgImage && bgImage.source_url ? bgImage.source_url : '';

		return (
			<div className="c-image-stats o-wrapper-padding">
				<RichText
					tagName="h1"
					className="c-image-stats__title o-h4"
					value={ attributes.title }
					onChange={ ( title ) => setAttributes( { title } ) }
					placeholder={ 'Enter block title' }
					withoutInteractiveFormatting
					allowedFormats={ [] }
				/>
				<figure className="c-image-stats__media">
					{ bgSrc && <img className="c-image-stats__image u-fit-contain" src={ bgSrc } alt="" /> }
					<MediaUploadCheck fallback={ instructions }>
						<MediaUpload
							onSelect={ ( image ) => {
								setAttributes( {
									img: image.id,
								} );
							} }
							allowedTypes={ ALLOWED_MEDIA_TYPES }
							value={ img }
							render={ ( { open } ) => img ?
								<Fragment>
									{ isSelected && <Fragment>
										<Button isSecondary onClick={ () => {
											setAttributes( {
												img: undefined,
											} );
										} }>
											Remove Image
										</Button></Fragment> }
								</Fragment> :
								( <Button isSecondary onClick={ open }>
									Add Image
								</Button> ) }
						/>
					</MediaUploadCheck>

					<RichText
						tagName="figcaption"
						className="c-image-stats__caption"
						value={ attributes.caption }
						onChange={ ( caption ) => setAttributes( { caption } ) }
						placeholder={ 'Enter image caption' }
						allowedFormats={ [ 'core/bold' ] }
					/>
				</figure>

				<div className="c-image-stats__stats">
					<InnerBlocks
						className="c-stats c-stats--matrix"
						allowedBlocks={ [ 'egap/stats-item' ] }
						template={ [ [ 'egap/stats-item' ], [ 'egap/stats-item' ], [ 'egap/stats-item' ], [ 'egap/stats-item' ] ] }
					/>
				</div>
			</div>
		);
	}
}

export default compose(
	withSelect( ( select, props ) => {
		const { getMedia } = select( 'core' );
		const { img } = props.attributes;

		return {
			bgImage: img ? getMedia( img ) : null,
		};
	} ),
)( ImageStatsEdit );
