<?php
/**
 * The Template for displaying staff post
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package egap
 */

global $post;

$context = \Timber\Timber::get_context();

$context['memberData'] = \Chisel\Helpers::getStaffMemberData(get_the_ID());

if ( post_password_required( $post->ID ) ) {
	\Timber\Timber::render( 'single-password.twig', $context );
} else {
	\Timber\Timber::render( array( 'single-staff.twig', 'single.twig' ), $context );
}
