<?php

namespace Chisel;

class Gutenberg
{
	public function __construct()
	{
		add_filter('render_block', [$this, 'render_block'], 10, 2);
		add_filter('render_block_data', [$this, 'render_block_data'], 10, 2);
		add_action('enqueue_block_editor_assets', array($this, 'enqueue_block_editor_assets'));
		$editor_styles_path = get_template_directory_uri() . '/' . ltrim(str_replace(get_template_directory_uri(), '',
				\Chisel\Helpers::revisionedPath('styles/editor.css')), '/');
		add_editor_style($editor_styles_path);
		add_action('init', array($this, 'register_dynamic_blocks'), 11);
		add_action('init', array($this, 'set_default_templates'));
		//add_filter('allowed_block_types', array($this, 'allowed_block_types'));
	}

	public function allowed_block_types($allowed_blocks)
	{
		return $allowed_blocks;
	}

	public function enqueue_block_editor_assets()
	{
		wp_enqueue_script(
			'egap-editor-scripts',
			\Chisel\Helpers::revisionedPath('scripts/editor.bundle.js'),
			array('wp-blocks', 'wp-dom-ready', 'wp-edit-post')
		);
	}

	public function render_block($block_content, $block)
	{
		$relative_path = 'templates/blocks/' . $block['blockName'] . '.twig';
		$template_file = get_template_directory() . '/' . $relative_path;

		if (file_exists($template_file)) {
			$block['blockContent'] = $block_content;
			$block_content         = \Timber\Timber::compile($relative_path, $block);
		} elseif (stripos($block['blockName'], 'egap/') === false
		          && preg_match('/(<[^>]* (id="?\'?([^"\']+)"?\'?)[^>]*>)/is', $block_content, $m)) {
			if (!preg_match('/(<input|<select|<textarea|o-anchor)/i', $m[1])) {
				$block_content = \Timber\Timber::compile('partials/anchor.twig',
						array('id' => $m[3])) . str_replace($m[2],
						'', $block_content);
			}
		} elseif ($block['blockName'] === 'core/paragraph') {
			if(stripos($block_content, '<p></p>') !== false) {
				$block_content =  '';
			} else {
				$block_content = str_replace('<p>', '<p class="o-generic-links">', $block_content);

				if(stripos($block_content, '<br></p>') !== false) {
					$block_content =  str_replace('<br></p>', '<br><br></p>', $block_content);
				}
			}
		}

		return $block_content;
	}

	public function render_block_data($block, $source_block)
	{
		if ($block['blockName'] === 'egap/page-title') {
			$block['attrs']['title'] = empty($block['attrs']['title']) ? get_the_title() : $block['attrs']['title'];
		} elseif ($block['blockName'] === 'egap/page-date') {
			$block['attrs']['date'] = get_the_date('', get_the_ID());
		} elseif ($block['blockName'] === 'egap/members') {
			Helpers::$hasOverlay = true;
			$block['attrs'] = array_merge($block['attrs'], \Chisel\Helpers::getMembersData());
		} elseif ($block['blockName'] === 'egap/staff') {
			Helpers::$hasOverlay = true;
			$block['attrs'] = array_merge($block['attrs'], \Chisel\Helpers::getStaffData());
		} elseif ($block['blockName'] === 'egap/resources') {
			$block['attrs'] = array_merge($block['attrs'], \Chisel\Helpers::getResourcesData());
		} elseif ($block['blockName'] === 'egap/design-registry') {
			$block['attrs'] = array_merge($block['attrs'], \Chisel\Helpers::getDesignRegistryData());
		} elseif ($block['blockName'] === 'egap/related-projects') {
			$block['attrs'] = array_merge($block['attrs'], \Chisel\Helpers::getRelatedProjects());
		} elseif ($block['blockName'] === 'egap/related-resources') {
			$block['attrs'] = array_merge($block['attrs'],
				\Chisel\Helpers::getRelatedResources(get_the_ID(), $block['attrs']));
		} elseif ($block['blockName'] === 'egap/breadcrumbs') {
			$breadcrumbs = array();

			if (get_post_type() === 'resource') {
				$resource_index = get_option('page_for_resources');

				if ($resource_index) {
					$breadcrumbs[] = array(
						'title' => get_the_title($resource_index),
						'url'   => get_the_permalink($resource_index),
					);
				}
			} else {
				$ascentors_ids = get_post_ancestors(get_the_ID());

				if ($ascentors_ids) {
					foreach ($ascentors_ids as $id) {
						$breadcrumbs[] = array(
							'title' => get_the_title($id),
							'url'   => get_the_permalink($id),
						);
					}

					$breadcrumbs = array_reverse($breadcrumbs);
				}
			}

			$block['attrs']['breadcrumbs'] = $breadcrumbs;
		} elseif ($block['blockName'] === 'core/paragraph') {
			$block['attrs']['className'] = 'o-generic-links';
		}

		return $block;
	}

	public function register_dynamic_blocks()
	{
		register_block_type(
			'egap/members',
			array(
				'attributes'      => array(
					'title'  => array(
						'type' => 'string',
					),
					'anchor' => array(
						'type' => 'string',
					),
				),
				'render_callback' => function ($attributes) {
					return '';
				},
			)
		);
		register_block_type(
			'egap/staff',
			array(
				'attributes'      => array(
					'title'  => array(
						'type' => 'string',
					),
					'anchor' => array(
						'type' => 'string',
					),
				),
				'render_callback' => function ($attributes) {
					return '';
				},
			)
		);

		register_block_type(
			'egap/resources',
			array(
				'attributes'      => array(
					'title'  => array(
						'type' => 'string',
					),
					'anchor' => array(
						'type' => 'string',
					),
				),
				'render_callback' => function ($attributes) {
					return '';
				},
			)
		);

		register_block_type(
			'egap/design-registry',
			array(
				'attributes'      => array(
					'title'  => array(
						'type' => 'string',
					),
					'anchor' => array(
						'type' => 'string',
					),
				),
				'render_callback' => function ($attributes) {
					return '';
				},
			)
		);

		register_block_type(
			'egap/related-projects',
			array(
				'attributes'      => array(
					'title'  => array(
						'type' => 'string',
					),
					'anchor' => array(
						'type' => 'string',
					),
				),
				'render_callback' => function ($attributes) {
					return '';
				},
			)
		);

		register_block_type(
			'egap/related-resources',
			array(
				'attributes'      => array(
					'title'    => array(
						'type' => 'string',
					),
					'anchor'   => array(
						'type' => 'string',
					),
					'types'    => array(
						'type'    => 'array',
						'default' => array(),
						'items' => array('type' => 'integer'),
					),
					'filterBy' => array(
						'type'    => 'string',
						'default' => '',
					),
					'limit' => array(
						'type'    => 'number',
						'default' => 0,
					),
					'limitResults' => array(
						'type'    => 'boolean',
						'default' => false,
					),
					'showRelated' => array(
						'type'    => 'boolean',
						'default' => true,
					),
					'viewAll' => array(
						'type'    => 'boolean',
						'default' => false,
					),
					'viewAllText' => array(
						'type'    => 'string',
						'default' => 'View More',
					),

				),
				'render_callback' => function ($attributes) {
					return '';
				},
			)
		);
	}

	public function set_default_templates() {
		$page_type_object           = get_post_type_object('page');
		$page_type_object->template = array(
			array(
				'egap/section',
				array(),
				array(
					array(
						'egap/section-column',
						array(),
						array(
							array('egap/breadcrumbs')
						)
					),
					array(
						'egap/section-columns',
						array(),
						array(
							array(
								'core/column',
								array('verticalAlignment' => 'bottom'),
								array(
									array('egap/page-title')
								)
							),
							array(
								'core/column',
								array('verticalAlignment' => 'bottom'),
								array(
									array('egap/submenu', array(), array(
										array('egap/submenu-item')
									))
								)
							),
						)
					),
					array(
						'egap/section-column',
						array(),
						array(
							array('core/spacer', array('height' => 60))
						)
					),
					array(
						'egap/section-columns',
						array(),
						array(
							array(
								'core/column',
								array(),
								array(
									array('core/paragraph')
								)
							),
							array(
								'core/column',
								array(),
								array(
									array(
										'core/paragraph',
										array('placeholder' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquet augue felis, vitae placerat sapien mollis a. Duis consequat semper orci, ac consequat risus porttitor a. Integer vel vehicula augue, eget convallis diam. Quisque finibus tempor porta. Pellentesque bibendum dapibus magna, vel vehicula ligula maximus nec. Quisque commodo mi nibh, ac convallis nunc varius sodales. Nullam elementum mi elit. Maecenas sit amet laoreet velit. Integer laoreet finibus congue. Mauris vel urna ac ipsum bibendum tempus vitae in lorem. Suspendisse in pulvinar elit, a luctus sem. Sed quis magna ut ligula euismod vehicula sed ac purus. Suspendisse gravida erat eros, ac pretium arcu lacinia quis. Ut consectetur eu velit eu fermentum. Pellentesque porta dictum metus, ut fermentum augue varius in. Ut id lectus sit amet mauris hendrerit suscipit et sed felis.')
									),
								)
							),
						)
					),
				)
			),
		);

		$post = get_post_type_object( 'post' );
		$post->template = array(
			array( 'egap/section', array(), array(
				array('egap/section-column', array(), array(
					array('core/paragraph', array('placeholder' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquet augue felis, vitae placerat sapien mollis a. Duis consequat semper orci, ac consequat risus porttitor a. Integer vel vehicula augue, eget convallis diam. Quisque finibus tempor porta. Pellentesque bibendum dapibus magna, vel vehicula ligula maximus nec. Quisque commodo mi nibh, ac convallis nunc varius sodales. Nullam elementum mi elit. Maecenas sit amet laoreet velit. Integer laoreet finibus congue. Mauris vel urna ac ipsum bibendum tempus vitae in lorem. Suspendisse in pulvinar elit, a luctus sem. Sed quis magna ut ligula euismod vehicula sed ac purus. Suspendisse gravida erat eros, ac pretium arcu lacinia quis. Ut consectetur eu velit eu fermentum. Pellentesque porta dictum metus, ut fermentum augue varius in. Ut id lectus sit amet mauris hendrerit suscipit et sed felis.')),
				)),
			))
		);
	}
}
