<?php

namespace Chisel;

use Timber\Timber;

/**
 * Class Helpers
 * @package Chisel
 *
 * Defines helper methods used by Chisel
 */
class Helpers
{
	static $manifest;
	private static $cache = array();
	public static $members = array();
	public static $hasOverlay = false;

	public static function isTimberActivated()
	{
		return class_exists('Timber\\Timber');
	}

	public static function addTimberAdminNotice()
	{
		add_action('admin_notices',
			function () {
				echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url(admin_url('plugins.php#timber')) . '">' . esc_url(admin_url('plugins.php')) . '</a></p></div>';
			}
		);
	}

	public static function setChiselEnv()
	{
		if (isset($_SERVER['HTTP_X_CHISEL_PROXY'])) {
			define('CHISEL_DEV_ENV', true);
		}
	}

	private static function initManifest()
	{
		if (file_exists(get_template_directory() . '/' . \Chisel\Settings::MANIFEST_PATH)) {
			static::$manifest = json_decode(
				file_get_contents(get_template_directory() . '/' . \Chisel\Settings::MANIFEST_PATH),
				true
			);
		}
	}

	public static function getManifest()
	{
		if (empty(static::$manifest)) {
			static::initManifest();
		}

		return static::$manifest;
	}

	public static function revisionedPath($asset)
	{
		$pathinfo = pathinfo($asset);

		if ( ! defined('CHISEL_DEV_ENV')) {
			$manifest = static::getManifest();
			if ( ! array_key_exists($pathinfo['basename'], $manifest)) {
				return 'FILE-NOT-REVISIONED';
			}

			return sprintf(
				'%s/%s%s/%s',
				get_template_directory_uri(),
				\Chisel\Settings::DIST_PATH,
				$pathinfo['dirname'],
				$manifest[$pathinfo['basename']]
			);
		} else {
			return sprintf(
				'%s/%s%s%s',
				get_template_directory_uri(),
				\Chisel\Settings::DIST_PATH,
				trim($asset, '/'),
				'?' . time()
			);
		}
	}

	public static function getRelatedContentIds($id, $args, $return_total = false)
	{
		$args = wp_parse_args($args, array(
			'post_type'      => 'page',
			'orderby'        => 'post_date',
			'posts_per_page' => -1,
			'fields'         => 'ids',
			'meta_query'     => array(
				'relation' => 'or'
			)
		));

		if ( ! empty($args['meta_field'])) {
			$args['meta_query'] = array_merge($args['meta_query'], array(
				array(
					'key'     => $args['meta_field'],
					'compare' => 'like',
					'value'   => '"' . $id . '"',
				)
			));

			unset($args['meta_field']);
		}

		global $sitepress;

		if ( ! empty($sitepress)) {
			$lang = ICL_LANGUAGE_CODE;
			$sitepress->switch_lang('en');
		}

		$query = new \WP_Query($args);
		$posts = $query->get_posts();

		if ( ! empty($sitepress)) {
			$sitepress->switch_lang($lang);
		}

		if ($return_total) {
			return array('ids' => $posts, 'total' => $query->found_posts);
		} else {
			return $posts;
		}
	}

	public static function getResourceTileData($id)
	{
		$type_terms = wp_get_post_terms($id, 'resource-type');
		$color      = '';
		$types      = array();

		if ( ! is_wp_error($type_terms) && isset($type_terms[0])) {
			$color = get_field('color', 'resource-type_' . $type_terms[0]->term_id);

			foreach ($type_terms as $type_term) {
				$types[] = $type_term->name;
			}
		}

		$external_link = get_field('external_link', $id);
		$url           = $external_link ? $external_link : get_the_permalink($id);

		$footer = array();

		$countries = wp_get_post_terms($id, 'country', array('fields' => 'names', 'orderby' => 'name'));

		if ( ! is_wp_error($countries)) {
			$footer[] = implode(', ', $countries);
		}

		$status = get_field('status', $id);

		if ($status) {
			$footer[] = $status;
		}

		return array(
			'url'    => $url,
			'types'  => $types,
			'color'  => $color,
			'title'  => get_the_title($id),
			'footer' => $footer,
		);
	}

	public static function getMemberData($id = null)
	{
		if ($id === null) {
			$id = get_the_ID();
		}

		$id = get_post_field('ID', $id);

		if ($id < 0) {
			return false;
		}

		$thematic_areas = wp_get_post_terms($id, 'member-thematic-area', array(
			'fields'     => 'id=>name',
			'hide_empty' => true,
		));

		$research_regions = wp_get_post_terms($id, 'research-region', array(
			'fields'     => 'id=>name',
			'hide_empty' => true,
		));

		$types = wp_get_post_terms($id, 'member-type', array(
			'fields'     => 'id=>name',
			'hide_empty' => true,
		));

		$related_members = array();

		if (is_wp_error($thematic_areas)) {
			$thematic_areas = array();
		}

		if (is_wp_error($research_regions)) {
			$research_regions = array();
		}

		if (is_wp_error($types)) {
			$types = array();
		}

		if ($thematic_areas || $research_regions) {
			global $sitepress;

			if ( ! empty($sitepress)) {
				$lang = ICL_LANGUAGE_CODE;
				$sitepress->switch_lang('en');
			}

			$related_member_ids = (new \WP_Query(array(
				'post_status'    => 'publish',
				'post_type'      => 'member',
				'fields'         => 'ids',
				'post__not_in'   => array($id),
				'orderby'        => 'rand',
				'posts_per_page' => 4,
				'tax_query'      => array(
					'relation' => 'or',
					array(
						'taxonomy' => 'thematic-area',
						'terms'    => array_keys($thematic_areas),
					),
					array(
						'taxonomy' => 'research-region',
						'terms'    => array_keys($research_regions),
					)
				),
			)))->get_posts();

			if ( ! empty($sitepress)) {
				$sitepress->switch_lang($lang);
			}

			foreach ($related_member_ids as $member_id) {
				$title = explode(' ', get_the_title($member_id));

				if (count($title) > 1) {
					$title = $title[0] . ' ' . $title[1][0] . '.';
				}

				$related_members[] = array(
					'thumbnail' => get_post_thumbnail_id($member_id),
					'id'        => $member_id,
					'title'     => $title,
				);
			}

		}

		$related_publication_ids = static::getRelatedContentIds($id, array(
			'post_type'      => 'resource',
			'meta_field'     => 'related_members',
			'tax_query'      => array(
				array(
					'taxonomy' => 'resource-type',
					'field'    => 'slug',
					'terms'    => 'publication',
				),
			),
			'posts_per_page' => 4,
		));

		$related_publications = array();

		foreach ($related_publication_ids as $related_publication_id) {
			$related_publications[] = static::getResourceTileData($related_publication_id);
		}

		$raw_content = get_post_field('post_content', $id);
		$content     = apply_filters('the_content', $raw_content);
		$excerpt     = strip_shortcodes($raw_content);
		$excerpt     = excerpt_remove_blocks($excerpt);
		$excerpt     = wp_trim_words($excerpt, 100,
			'&hellip; <button class="o-read-more u-fw-700">' . __('Read More', 'egap') . '</button>');
		$email       = get_field('email', $id);

		return array(
			'id'                   => $id,
			'website'              => get_field('website', $id),
			'email'                => $email ? ('mailto:' . $email) : '',
			'affiliation'          => get_field('affiliation', $id),
			'thumbnail'            => get_post_thumbnail_id($id),
			'title'                => get_the_title($id),
			'content'              => $content,
			'excerpt'              => $excerpt,
			'related_members'      => $related_members,
			'related_publications' => $related_publications,
			'thematic_areas'       => array_values($thematic_areas),
			'research_regions'     => array_values($research_regions),
			'types'                => array_values($types),
		);
	}

	public static function getStaffMemberData($id = null)
	{
		if ($id === null) {
			$id = get_the_ID();
		}

		$id = get_post_field('ID', $id);

		if ($id < 0) {
			return false;
		}

		$positions = wp_get_post_terms($id, 'staff-position', array(
			'fields'     => 'id=>name',
			'hide_empty' => true,
		));

		$related_members = array();

		if (is_wp_error($positions)) {
			$positions = array();
		}

		if ($positions) {
			global $sitepress;

			if ( ! empty($sitepress)) {
				$lang = ICL_LANGUAGE_CODE;
				$sitepress->switch_lang('en');
			}

			$related_member_ids = (new \WP_Query(array(
				'post_status'    => 'publish',
				'post_type'      => 'staff',
				'fields'         => 'ids',
				'post__not_in'   => array($id),
				'orderby'        => 'rand',
				'posts_per_page' => 4,
				'tax_query'      => array(
					'relation' => 'or',
					array(
						'taxonomy' => 'staff-position',
						'terms'    => array_keys($positions),
					),
				),
			)))->get_posts();

			if ( ! empty($sitepress)) {
				$sitepress->switch_lang($lang);
			}

			foreach ($related_member_ids as $member_id) {
				$title = explode(' ', get_the_title($member_id));

				if (count($title) > 1) {
					$title = $title[0] . ' ' . $title[1][0] . '.';
				}

				$related_members[] = array(
					'thumbnail' => get_post_thumbnail_id($member_id),
					'id'        => $member_id,
					'title'     => $title,
				);
			}

		}

		$related_publication_ids = static::getRelatedContentIds($id, array(
			'post_type'      => 'resource',
			'meta_field'     => 'related_staff',
			'tax_query'      => array(
				array(
					'taxonomy' => 'resource-type',
					'field'    => 'slug',
					'terms'    => 'publication',
				),
			),
			'posts_per_page' => 4,
		));

		$related_publications = array();

		foreach ($related_publication_ids as $related_publication_id) {
			$related_publications[] = static::getResourceTileData($related_publication_id);
		}

		$raw_content = get_post_field('post_content', $id);
		$content     = apply_filters('the_content', $raw_content);
		$excerpt     = strip_shortcodes($raw_content);
		$excerpt     = excerpt_remove_blocks($excerpt);
		$excerpt     = wp_trim_words($excerpt, 100,
			'&hellip; <button class="o-read-more u-fw-700">' . __('Read More', 'egap') . '</button>');
		$email       = get_field('email', $id);

		return array(
			'id'                   => $id,
			'website'              => get_field('website', $id),
			'email'                => $email ? ('mailto:' . $email) : '',
			'affiliation'          => get_field('affiliation', $id),
			'thumbnail'            => get_post_thumbnail_id($id),
			'title'                => get_the_title($id),
			'content'              => $content,
			'excerpt'              => $excerpt,
			'related_members'      => $related_members,
			'related_publications' => $related_publications,
			'positions'            => $positions,
		);
	}

	private static function parseFilterValues($filter_names)
	{
		$filters = array();

		foreach ($filter_names as $filter_name) {
			$temp_values = ! empty($_REQUEST[$filter_name]) ? explode(',', $_REQUEST[$filter_name]) : array();
			$values      = array();

			foreach ($temp_values as $value) {
				$value = intval($value);

				if ($value) {
					$values[] = $value;
				}
			}

			if ($values) {
				$filters[$filter_name] = $values;
			}
		}

		return $filters;
	}

	private static function getFilteredPosts($post_name, $filters, $parameters = array())
	{
		$default_parameters = array(
			'offset'         => ! empty($_REQUEST['offset']) ? (int)$_REQUEST['offset'] : 0,
			'orderby'        => ! empty($_REQUEST['orderby']) ? sanitize_key($_REQUEST['orderby']) : 'date',
			'order'          => ! empty($_REQUEST['order']) ? sanitize_key($_REQUEST['order']) : 'DESC',
			'posts_per_page' => ! empty($_REQUEST['posts_per_page']) ? (int)$_REQUEST['posts_per_page'] : 24,
			'search'         => ! empty($_REQUEST['search']) ? sanitize_text_field($_REQUEST['search']) : '',
		);

		$parameters = wp_parse_args($parameters, $default_parameters);

		$tax_query = array('relation' => 'and');

		foreach ($filters as $filter_name => $filter_values) {
			$tax_query[] = array(
				'taxonomy' => $filter_name,
				'terms'    => $filter_values,
			);
		}

		$query_settings = array(
			'post_status'    => 'publish',
			'post_type'      => $post_name,
			'fields'         => 'ids',
			'posts_per_page' => $parameters['posts_per_page'],
			'offset'         => $parameters['offset'],
			's'              => $parameters['search'],
			'orderby'        => $parameters['orderby'],
			'order'          => $parameters['order'],
			'tax_query'      => $tax_query,
		);

		global $sitepress;

		if ( ! empty($sitepress)) {
			$lang = ICL_LANGUAGE_CODE;
			$sitepress->switch_lang('en');
		}

		$query = new \WP_Query($query_settings);
		$ids   = $query->get_posts();

		if ( ! empty($sitepress)) {
			$sitepress->switch_lang($lang);
		}

		return array('ids' => $ids, 'total' => $query->found_posts);
	}

	private static function getFilterTerms($filter_taxonomies, $filters, $post_type)
	{
		$terms            = get_terms(array(
			'post_types' => $post_type,
			'taxonomy'   => array_keys($filter_taxonomies),
			'hide_empty' => true,
		));
		$taxonomies       = array();
		$selected_filters = array();

		foreach ($filters as $filter_values) {
			$selected_filters = array_merge($filter_values);
		}

		foreach ($filter_taxonomies as $taxonomy_name => $taxonomy_label) {
			$taxonomies[$taxonomy_name] = array('title' => $taxonomy_label, 'terms' => array(), 'selected' => array());
		}

		if ($terms) {
			foreach ($terms as $term) {
				$selected = in_array($term->term_id, $selected_filters);

				if ($selected) {
					$taxonomies[$term->taxonomy]['terms'][] = $term->name;
				}

				$taxonomies[$term->taxonomy]['terms'][] = array(
					'id'       => $term->term_id,
					'title'    => $term->name,
					'selected' => $selected,
				);
			}
		}

		return $taxonomies;
	}

	public static function getMembersData($parameters = array(), $filters = array())
	{
		$filter_taxonomies = array(
			'member-type'          => 'By type',
			'member-thematic-area' => 'Thematic area',
			'research-region'      => 'Research region',
			'member-location'      => 'Member location'
		);
		$filters           = $filters ? $filters : static::parseFilterValues(array_keys($filter_taxonomies));
		$parameters        = wp_parse_args($parameters, array('orderby' => 'title', 'order' => 'ASC'));
		$post_data         = static::getFilteredPosts('member', $filters, $parameters);
		$posts             = array();

		foreach ($post_data['ids'] as $post_id) {
			$posts[] = array(
				'thumbnail'   => get_post_thumbnail_id($post_id),
				'title'       => get_the_title($post_id),
				'id'          => $post_id,
				'affiliation' => get_field('affiliation', $post_id),
				'website'     => get_field('website', $post_id),
			);
		}

		$taxonomies = static::getFilterTerms($filter_taxonomies, $filters, 'member');

		return array(
			'total'      => $post_data['total'],
			'taxonomies' => $taxonomies,
			'posts'      => $posts,
		);
	}

	public static function getStaffData($parameters = array(), $filters = array())
	{
		$filter_taxonomies = array(
			'staff-position' => 'By position',
		);
		$filters           = $filters ? $filters : static::parseFilterValues(array_keys($filter_taxonomies));
		$parameters        = wp_parse_args($parameters, array('orderby' => 'title', 'order' => 'ASC'));
		$post_data         = static::getFilteredPosts('staff', $filters, $parameters);
		$posts             = array();

		foreach ($post_data['ids'] as $post_id) {
			$posts[] = array(
				'thumbnail'   => get_post_thumbnail_id($post_id),
				'title'       => get_the_title($post_id),
				'id'          => $post_id,
				'affiliation' => get_field('affiliation', $post_id),
				'website'     => get_field('website', $post_id),
			);
		}

		$taxonomies = static::getFilterTerms($filter_taxonomies, $filters, 'staff');

		return array(
			'total'      => $post_data['total'],
			'taxonomies' => $taxonomies,
			'posts'      => $posts,
		);
	}

	public static function getResourcesData($parameters = array(), $filters = array())
	{
		$filter_taxonomies = array(
			'resource-type'   => 'By type',
			'thematic-area'   => 'Thematic area',
			'research-region' => 'Research region',
		);
		$filters           = $filters ? $filters : static::parseFilterValues(array_keys($filter_taxonomies));
		$parameters        = wp_parse_args($parameters);
		$post_data         = static::getFilteredPosts('resource', $filters, $parameters);
		$posts             = array();

		$colors = array();

		foreach ($post_data['ids'] as $post_id) {
			$type_terms = wp_get_post_terms($post_id, 'resource-type');
			$types      = array();
			$color      = '';

			if ( ! is_wp_error($type_terms) && isset($type_terms[0])) {
				$color                           = get_field('color', 'resource-type_' . $type_terms[0]->term_id);
				$colors[$type_terms[0]->term_id] = $color;

				foreach ($type_terms as $type_term) {
					$types[] = $type_term->name;
				}
			}

			$external_link = get_field('external_link', $post_id);
			$url           = $external_link ? $external_link : get_the_permalink($post_id);

			$posts[] = array(
				'thumbnail'  => get_post_thumbnail_id($post_id),
				'title'      => get_the_title($post_id),
				'id'         => $post_id,
				'url'        => $url,
				'color'      => $color,
				'types'      => $types,
				'subheading' => get_field('subheading', $post_id),
			);
		}

		$taxonomies = static::getFilterTerms($filter_taxonomies, $filters, 'resource');

		return array(
			'total'      => $post_data['total'],
			'taxonomies' => $taxonomies,
			'posts'      => $posts,
		);
	}

	private static function orderByCountries($a, $b)
	{
		$ca = ! empty($a['countries']) ? $a['countries'][0] : '';
		$cb = ! empty($b['countries']) ? $b['countries'][0] : '';

		return strcmp($ca, $cb);
	}

	public static function getRelatedProjects($post_id = null)
	{
		$post_id = ! $post_id ? get_the_ID() : (int)$post_id;

		$post_id = get_post_field('ID', $post_id);

		$data = array('posts' => array());

		if ($post_id < 0) {
			return $data;
		}

		$ids = static::getRelatedContentIds($post_id, array(
			'post_type'  => 'project',
			'meta_field' => 'related_pages',
			'tax_query'  => array(
				array(
					'taxonomy' => 'country',
					'operator' => 'exists',
				),
			),
		));

		$posts = array();

		foreach ($ids as $id) {
			$country_terms = wp_get_post_terms($id, 'country', array('orderby' => 'name', 'fields' => 'names'));
			$countries     = array();

			if ($country_terms && ! is_wp_error($country_terms)) {
				$countries = $country_terms;
			}

			$related_member_ids = get_field('related_members', $id);

			if (empty($related_member_ids)) {
				$related_member_ids = array();
			}

			$related_staff_ids = get_field('related_staff', $id);

			if (empty($related_staff_ids)) {
				$related_staff_ids = array();
			}

			$related_member_ids = array_merge($related_member_ids, $related_staff_ids);
			$members            = array();

			if ( ! empty($related_member_ids)) {
				foreach ($related_member_ids as $related_member_id) {
					$members[] = array(
						'thumbnail' => get_post_thumbnail_id($related_member_id),
						'title'     => get_the_title(),
					);
				}
			}

			$posts[] = array(
				'url'       => get_permalink($id),
				'title'     => get_the_title($id),
				'countries' => $countries,
				'members'   => $members,
			);
		}

		// usort($posts, array(static::class, 'orderByCountries'));

		$data['posts'] = $posts;

		return $data;
	}

	public static function getRelatedResources($post_id = null, $parameters = array())
	{
		$parameters = wp_parse_args($parameters, array(
			'filterBy'     => '',
			'types'        => array(),
			'limit'        => 0,
			'limitResults' => false,
			'showRelated'  => true,
			'viewAll'      => false,
			'viewAllText'  => 'View All',
		));

		$post_id            = ! $post_id ? get_the_ID() : (int)$post_id;
		$post_id            = get_post_field('ID', $post_id);
		$page_for_resources = get_option('page_for_resources');
		$related_url        = '';
		$types              = array_filter($parameters['types']);

		if ($page_for_resources) {
			$related_url = add_query_arg(array('resource-type' => implode(',', $types)),
				get_permalink($page_for_resources));
		}

		$data = array(
			'posts'       => array(),
			'limit'       => $parameters['limit'],
			'viewAll'     => $parameters['viewAll'],
			'viewAllLink' => $related_url,
			'viewAllText' => $parameters['viewAllText'] ? $parameters['viewAllText'] : 'View All'
		);

		if ($post_id < 0) {
			return $data;
		}

		$tax_query       = array('relation' => 'AND');
		$filter_taxonomy = ! empty($parameters['filterBy']) ? $parameters['filterBy'] : '';

		if ($filter_taxonomy) {
			$tax_query[] = array(
				'taxonomy' => $filter_taxonomy,
				'operator' => 'exists',
			);

			$data['filterBy'] = $parameters['filterBy'];
		}

		if ( ! empty($types)) {
			$tax_query[] = array(
				'taxonomy' => 'resource-type',
				'terms'    => $types,
			);
		}

		$post_type_plural = 'related_' . get_post_type($post_id) . 's';

		$related_result = static::getRelatedContentIds($post_id, array(
			'post_type'      => 'resource',
			'meta_field'     => $parameters['showRelated'] ? $post_type_plural : '',
			'posts_per_page' => $parameters['limitResults'] && (int)$parameters['limit'] ? (int)$parameters['limit'] : -1,
			'tax_query'      => $tax_query,
		), true);

		$posts        = array();
		$filter_terms = array();

		foreach ($related_result['ids'] as $id) {
			$post_data = static::getResourceTileData($id);

			if ($filter_taxonomy) {
				$terms = wp_get_post_terms($id, $filter_taxonomy, array('orderby' => 'name', 'fields' => 'id=>name'));

				if ($terms && ! is_wp_error($terms)) {
					$filter_terms        = $filter_terms + $terms;
					$post_data['filter'] = array_keys($terms);
				}
			}

			$posts[] = $post_data;
		}

		if ($filter_terms) {
			asort($filter_terms);
			$data['filter'] = $filter_terms;
		}

		$data['posts'] = $posts;
		$data['total'] = $related_result['total'];

		return $data;
	}

	public static function getDesignRegistryData($parameters = array())
	{
		$parameters           = array_merge($_REQUEST, $parameters);
		$default_parameters   = array(
			'search'  => '',
			'from'    => 0,
			'size'    => 20,
			'sort_by' => 'newest',
			'post_id' => get_the_ID(),
		);
		$parameters           = wp_parse_args($parameters, $default_parameters);
		$parameters['search'] = wp_unslash($parameters['search']);
		$post_id              = (int)$parameters['post_id'];

		$posts_query = new \WP_Query(array(
			'post_type'      => 'registration',
			'posts_per_page' => $parameters['size'],
			'offset'         => max(0, ($parameters['from'] - 1) * $parameters['size']),
			'orderby'        => 'title',
			'order'          => $parameters['sort_by'] === 'newest' ? 'desc' : 'asc',
			's'              => $parameters['search'],
		));

		foreach ($posts_query->get_posts() as $post) {
			$post_data = json_decode($post->post_content, true);

			if ( ! empty($post_data)) {
				$footer = array();

				if ($post->post_date !== '0000-00-00 00:00:00') {
					$footer[] = date('F j, Y', strtotime($post->post_date));
				}

				if ( ! empty($post->post_title)) {
					$footer[] = $post->post_title;
				}

				$posts[] = array(
					'id'      => $post_data['link_id'],
					'url'     => $post_data['url'],
					'title'   => $post_data['title'],
					'authors' => $post_data['authors'],
					'links'   => $post_data['links'],
					'footer'  => $footer,
				);
			}
		}

		$base     = get_the_permalink($post_id);
		$add_args = array();

		if ($default_parameters['search'] !== $parameters['search']) {
			$add_args['search'] = $parameters['search'];
		}

		if ($default_parameters['sort_by'] !== $parameters['sort_by']) {
			$add_args['sort_by'] = $parameters['sort_by'];
		}

		return array(
			'items'      => $posts,
			'search'     => sanitize_text_field($parameters['search']),
			'sort_by'    => $parameters['sort_by'],
			'count'      => sprintf(_n('%s Registration', '%s Registrations', $posts_query->found_posts),
				number_format($posts_query->found_posts)),
			'pagination' => paginate_links(array(
				'base'      => $base . '%_%',
				'format'    => (stripos($base, '?') === false ? '?' : '&') . 'from=%#%',
				'total'     => ceil($posts_query->found_posts / $parameters['size']),
				'current'   => max(1, (int)$parameters['from']),
				'end_size'  => 2,
				'mid_size'  => 4,
				'prev_text' => '«',
				'next_text' => '»',
				'add_args'  => $add_args,
			)),
		);
	}

	public static function parse_member_links($content)
	{
		return preg_replace_callback('/(href="([^"]*(\/member\/|member=)[^"]*)")/is', function ($m) {
			if (($member_id = Helpers::add_member($m[2]))) {
				static::$hasOverlay = true;

				return 'href="#" data-member-trigger="' . $member_id . '"';
			}

			return $m[0];
		}, $content);
	}

	public static function parse_staff_links($content)
	{
		return preg_replace_callback('/(href="([^"]*(\/staff\/|staff=)[^"]*)")/is', function ($m) {
			if (($member_id = Helpers::add_staff($m[2]))) {
				static::$hasOverlay = true;

				return 'href="#" data-staff-trigger="' . $member_id . '"';
			}

			return $m[0];
		}, $content);
	}

	public static function add_member($member_id, $reset_time = 0, $auto_show = false)
	{
		return static::add_member_staff('member', $member_id, $reset_time, $auto_show);
	}

	public static function add_staff($member_id, $reset_time = 0, $auto_show = false)
	{
		return static::add_member_staff('staff', $member_id, $reset_time, $auto_show);
	}

	private static function add_member_staff($type, $member_id, $reset_time = 0, $auto_show = false)
	{
		$member_hash = md5($member_id);

		$cache_parsed_key = $type . '_' . $member_hash . '_' . (int)$reset_time . '_' . (int)$auto_show;

		if (isset(static::$cache[$cache_parsed_key])) {
			return static::$cache[$cache_parsed_key];
		}

		if ( ! is_numeric($member_id)) {
			$cache_key = $type . '_url_' . $member_hash;

			if ( ! isset(static::$cache[$cache_key])) {
				static::$cache[$cache_key] = url_to_postid($member_id);
			}

			$member_id = static::$cache[$cache_key];
		}

		$member_id = (int)$member_id;

		if ( ! $member_id) {
			static::$cache[$cache_parsed_key] = false;

			return false;
		}

		if (isset(static::$members[$member_id])) {
			static::$cache[$cache_parsed_key] = $member_id;

			return $member_id;
		}

		$cache_key = $type . '_id_' . $member_hash;

		if ( ! isset(static::$cache[$cache_key])) {
			global $wpdb;
			static::$cache[$cache_key] = $wpdb->get_var("select ID
			from {$wpdb->posts}
			where ID = " . $member_id . "
			and post_type = " . $wpdb->prepare('%s', $type) . "
			and post_status = 'publish'");
		}

		if ( ! static::$cache[$cache_key]) {
			static::$cache[$cache_parsed_key] = false;

			return false;
		}

		static::$members[$member_id] = array(
			'id' => $member_id,
		);

		static::$cache[$cache_parsed_key] = $member_id;

		return $member_id;
	}
}
