<?php

namespace Chisel;

use Timber\Timber;

/**
 * Class Site
 * @package Chisel
 *
 * Use this class to setup whole site related configuration
 */
class Site extends \Timber\Site
{
	/**
	 * Site constructor.
	 */
	public function __construct()
	{
		// set default twig templates directory
		Timber::$dirname = Settings::TEMPLATES_DIR;

		$this->chiselInit();

		parent::__construct();
	}

	/**
	 * Initiate chisel configuration.
	 */
	public function chiselInit()
	{
		add_filter('timber_context', array($this, 'addToContext'));
		add_filter('Timber\PostClassMap', array('\Chisel\Post', 'overrideTimberPostClass'));

		register_nav_menus([
			'navigation-top' => __('Main Menu'),
			'footer-1'       => __('Footer Column 1'),
			'footer-2'       => __('Footer Column 2'),
			'footer-3'       => __('Footer Column 3'),
		]);

		add_filter('acf/validate_post_id', array('\Chisel\Site', 'validate_post_id'), 10, 2);
	}

	/**
	 * You can add custom global data to twig context
	 *
	 * @param array $context
	 *
	 * @return array
	 */
	public static function addToContext($context)
	{
		global $sitepress;

		if(!empty($sitepress)) {
			$lang = ICL_LANGUAGE_CODE;
			$sitepress->switch_lang('en');
		}

		$context['main_nav']    = new \Timber\Menu('navigation-top');
		$context['footer_navs'] = array_unique(array(
			new \Timber\Menu('footer-1'),
			new \Timber\Menu('footer-2'),
			new \Timber\Menu('footer-3'),
		));

		if(!empty($sitepress)) {
			$sitepress->switch_lang($lang);
		}

		$context['post']        = Timber::get_post();
		$context['site_js']     = array(
			'ajax_url' => admin_url('admin-ajax.php'),
			'post_id'  => get_the_ID(),
		);

		if (function_exists('get_field')) {
			$context['social_links']            = get_field('social_links', 'options');
			$context['footer_copy']             = get_field('footer_copy', 'options');
			$context['footer_newsletter_show']  = get_field('footer_newsletter_show', 'options');
			$context['footer_newsletter_text']  = get_field('footer_newsletter_text', 'options');
			$context['footer_newsletter_title'] = get_field('footer_newsletter_title', 'options');
			$context['mailchimp_api_key']       = get_field('mailchimp_api_key', 'options');
			$context['mailchimp_list_url']      = get_field('mailchimp_list_url', 'options');
			$context['mailchimp_service_url']   = get_field('mailchimp_service_url', 'options');
			$context['mailchimp_username']      = get_field('mailchimp_username', 'options');
			$context['mailchimp_audience_id']   = get_field('mailchimp_audience_id', 'options');
		}

		return $context;
	}

	public static function validate_post_id($post_id, $original_post_id)
	{
		if (strpos($post_id, 'options_') === 0) {
			$post_id = 'options';
		}

		return $post_id;
	}
}
