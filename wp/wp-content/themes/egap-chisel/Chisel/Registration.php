<?php


namespace Chisel;


class Registration
{
	static $designRegistryTemplate = array(
		'size'  => 10,
		'from'  => 0,
		'query' =>
			array(
				'bool' =>
					array(
						'must'   =>
							array(
								'query_string' =>
									array(
										'query' => '*',
									),
							),
						'filter' =>
							array(
								array(
									'terms' =>
										array(
											'sources' =>
												array('OSF'),
										),
								),
								array(
									'terms' =>
										array(
											'registration_type' =>
												array(
													'EGAP Registration',
												),
										),
								),
								array(
									'terms' =>
										array(
											'types' =>
												array(
													'registration',
												),
										),
								),
							),
					),
			),
	);

	public static function prepareQuery($user_options = array()) {
		$default_options = array(
			'from' => 0,
			'size' => 20,
			'search' => '*',
			'sort_by' => 'date_published',
			'order_by' => 'desc',
		);
		$options           = wp_parse_args($user_options, $default_options);

		$search_template = static::$designRegistryTemplate;
		$search_template['from'] = $options['from'];
		$search_template['size'] = $options['size'];
		$search_template['query']['bool']['must']['query_string']['query'] = $options['search'];
		$search_template['sort'][$default_options['sort_by']] = $default_options['order_by'];

		return $search_template;
	}

	public static function search($query = array()) {
		$request = wp_remote_post('https://share.osf.io/api/v2/search/creativeworks/_search', array(
			'body'        => json_encode(empty($query) ? static::prepareQuery() : $query),
			'httpversion' => '1.1',
			'headers'     => array('Content-Type' => 'application/json'),
			'sslverify'   => false,
		));

		if ( ! is_wp_error($request)) {
			return json_decode(wp_remote_retrieve_body($request), true);
		}

		return false;
	}

	public static function parse_hit($hit) {
		$identifiers = $hit['_source']['identifiers'];
		$url         = '';
		$links       = array();
		$link_id = '';

		foreach ($identifiers as $identifier) {
			if (empty($hit_id)) {
				if (preg_match('/api\.osf\.io\/v2\/registrations\/([^\/]+)\/$/', $identifier, $m) > 0
				    || preg_match('/osf\.io\/([^\/]+)\/$/', $identifier, $m) > 0) {
					$link_id = $m[1];
				}
			}

			if (preg_match('/\/\/share\/io\.osf\.registrations:(.+)$/', $identifier) === 1) {
				$identifier = 'https://share.osf.io/registration/' . $hit['_source']['id'];
			}
			$links[] = array('title' => $identifier, 'url' => $identifier);
			if (stripos($identifier, '//osf.io') !== false) {
				$url = $identifier;
				break;
			}
		}

		$data = array(
			'id' => $hit['_source']['id'],
			'link_id'     => $link_id,
			'url'    => $url,
			'title'  => $hit['_source']['title'],
			'date_published' => strtotime($hit['_source']['date_published']),
		);

		$contributors = $hit['_source']['lists']['contributors'];

		foreach ($contributors as $index => $contributor) {
			if (empty($contributor['order_cited'])) {
				unset($contributors[$index]);
			}
		}

		uasort($contributors, array(Registration::class, 'contributors_compare'));

		$authors = array();

		foreach ($contributors as $contributor) {
			$identifiers = $contributor['identifiers'];
			$link        = '';

			foreach ($identifiers as $identifier) {
				if (stripos($identifier, 'osf.io') !== false) {
					$link = $identifier;
					break;
				}
			}

			$authors[] = array('title' => $contributor['name'], 'url' => $link);
		}

		$data['authors'] = $authors;
		$data['links']   = $links;

		return $data;
	}

	public static function getDetails($ids, $limit = 20) {
		$url = 'https://api.osf.io/v2/sparse/registrations/?filter%5Bid%5D=' . implode(',',
				$ids) . '&format=json&page%5Bsize%5D=' . $limit;

		$ids_request = wp_remote_get($url, array(
			'httpversion' => '1.1',
			'headers'     => array('Content-Type' => 'application/json'),
			'sslverify'   => false,
		));

		if ( ! is_wp_error($ids_request)) {
			$ids_request_data =  json_decode(wp_remote_retrieve_body($ids_request), true);
			$result = array();

			if ( ! empty($ids_request_data['data'])) {
				foreach ($ids_request_data['data'] as $datum) {
					$original_id = !empty($datum['attributes']['registered_meta']['q3']['value']) ? $datum['attributes']['registered_meta']['q3']['value'] : '';

					$registration_data = array(
						'original_id' => '-',
						'original_date' => '0000-00-00 00:00:00',
						'original_date_gmt' => '0000-00-00 00:00:00',
					);

					if(!empty($original_id)) {
						$registration_data['original_id'] = $original_id;

						$date = date_create_from_format('Ymd', preg_replace('/[a-z]+/i', '', $original_id));

						if($date) {
							$registration_data['original_date'] = $registration_data['original_date_gmt'] = $date->format('Y-m-d 00:00:00');
						}
					}

					$result[$datum['id']] = $registration_data;
				}
			}

			return $result;
		} else {
			return false;
		}
	}

	private static function contributors_compare($a, $b)
	{
		if ($a['order_cited'] === $b['order_cited']) {
			return 0;
		}

		return $a['order_cited'] > $b['order_cited'] ? 1 : -1;
	}
}
