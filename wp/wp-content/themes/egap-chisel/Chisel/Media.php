<?php

namespace Chisel;

/**
 * Class Media
 * @package Chisel
 *
 * Default media settings for Chisel
 */
class Media {

	public static $imageSizes = array(
		'circle'  => array(
			'width'        => 142,
			'height'       => 142,
			'crop'         => true,
		),
		'circle@2x'  => array(
			'width'        => 2*142,
			'height'       => 2*142,
			'crop'         => true,
		),
		'circle-small'  => array(
			'width'        => 40,
			'height'       => 40,
			'crop'         => true,
		),
		'circle-small@2x'  => array(
			'width'        => 2*40,
			'height'       => 2*40,
			'crop'         => true,
		),
		// large - 1024
		'w1366' => array(
			'width'        => 1366,
			'height'       => 0,
			'crop'         => false,
		),
		'w1920' => array(
			'width'        => 1920,
			'height'       => 0,
			'crop'         => false,
		),
		'w2560' => array(
			'width'        => 2560,
			'height'       => 0,
			'crop'         => false,
		),
	);

	public static $sizesAttr = array(
	);

	public function __construct() {
		$this->addImagesSizes();
		// add_action( 'after_setup_theme', array( $this, 'defaultMediaSetting' ) );
		add_filter( 'image_size_names_choose', array( $this, 'customImageSizes' ) );
		add_action( 'jpeg_quality', array( $this, 'customJpegQuality' ) );
		add_filter( 'oembed_dataparse', array( $this, 'customOembedFilter' ), 10, 4 );
		add_filter( 'sanitize_file_name', array( $this, 'filterSanitizeFileName' ), 10 );
		add_filter( 'max_srcset_image_width', array( $this, 'maxSrcsetImageWidth' ), 10, 0 );
	}

	/**
	 * Use this method to register custom image sizes
	 */
	public function addImagesSizes() {
		foreach ( static::$imageSizes as $imageSlug => $imageSize ) {
			add_image_size( $imageSlug, $imageSize['width'], $imageSize['height'], $imageSize['crop'] );
		}
	}

	/**
	 * Add custom image sizes option to WP admin
	 *
	 * @param  array $sizes Default sizes
	 *
	 * @return array        Updated sizes
	 */
	public function customImageSizes( $sizes ) {
		if(isset($sizes['thumbnail'])) {
			unset($sizes['thumbnail']);
		}
		if(isset($sizes['medium'])) {
			unset($sizes['medium']);
		}
		if(isset($sizes['large'])) {
			unset($sizes['large']);
		}
		$sizes['w2560'] = __( 'Large' );
		return $sizes;
	}

	/**
	 * Default settings when adding or editing post images
	 */
	public function defaultMediaSetting() {
		update_option( 'image_default_align', 'center' );
		update_option( 'image_default_link_type', 'none' );
		update_option( 'image_default_size', 'full' );
	}

	/**
	 * Sets custom JPG quality when resizing images
	 * @return number JPG Quality
	 */
	public function customJpegQuality() {
		return 97;
	}

	/**
	 * Custom container for embed content
	 *
	 * oembed_dataparse filter runs when the data is gathered from the oembed provider.
	 * If you make changes to this filter, already embedded data won't change.
	 * You need to embed them again or use embed_oembed_html filter which is less performant
	 * and doesn't provide $data object
	 *
	 * @param $html
	 * @param $data
	 *
	 * @return string
	 */
	function customOembedFilter( $html, $data ) {
		if ( ! is_object( $data ) || empty( $data->type ) ) {
			return $html;
		}

		return '<div class="c-embed c-embed--' . strtolower( $data->type ) . '">' . $html . '</div>';
	}

	public function filterSanitizeFileName( $filename ) {
		$info = pathinfo( $filename );
		$ext  = empty( $info['extension'] ) ? '' : '.' . $info['extension'];

		$name = str_replace( $ext, '', $filename );

		return sanitize_title( strtolower( $name ) ) . $ext;
	}

	public function maxSrcsetImageWidth() {
		return 2560;
	}
}
