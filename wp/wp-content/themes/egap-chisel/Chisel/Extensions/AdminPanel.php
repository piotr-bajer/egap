<?php

namespace Chisel\Extensions;

class AdminPanel implements ChiselExtension
{
	public function extend()
	{
		add_action('admin_init', array($this, 'admin_init'));
		add_action('admin_init', array($this, 'register_settings'));
		add_action('rest_api_init', array($this, 'register_settings'));
		add_filter('display_post_states', array($this, 'display_post_states'), 10, 2);

		$taxonomies = array(
			'member-type',
			'thematic-area',
			'member-thematic-area',
			'research-region',
			'member-location',
			'resource-type',
			'country'
		);

		foreach ($taxonomies as $tax) {
			add_filter('manage_' . $tax . '_custom_column', array($this, 'manage_term_custom_column'), 10, 3);
			add_filter('manage_edit-' . $tax . '_columns', array($this, 'manage_edit_term_custom_columns'), 10, 1);
		}
	}

	public function register_settings()
	{
		register_setting(
			'reading',
			'page_for_resources',
			array(
				'type'              => 'integer',
				'sanitize_callback' => 'intval',
				'default'           => null,
				'show_in_rest'      => true,
			)
		);

		register_setting(
			'reading',
			'page_for_members',
			array(
				'type'              => 'integer',
				'sanitize_callback' => 'intval',
				'default'           => null,
				'show_in_rest'      => true,
			)
		);
	}

	public function admin_init()
	{
		add_settings_field(
			'page_for_resources',
			__('Page for Resources', 'egap'),
			array($this, 'page_for_resources_callback'),
			'reading',
			'default',
			array('label_for' => 'page_for_resources')
		);

		add_settings_field(
			'page_for_members',
			__('Page for Members', 'egap'),
			array($this, 'page_for_members_callback'),
			'reading',
			'default',
			array('label_for' => 'page_for_members')
		);
	}

	public function page_for_resources_callback()
	{
		$resources_page_id = get_option('page_for_resources');

		$items = get_posts(array(
			'posts_per_page' => -1,
			'orderby'        => 'name',
			'order'          => 'ASC',
			'post_type'      => 'page',
		));

		echo '<select id="page_for_resources" name="page_for_resources">';
		echo '<option value="0">' . __('— Select —', 'wordpress') . '</option>';

		foreach ($items as $item) {
			$selected = ($resources_page_id == $item->ID) ? 'selected="selected"' : '';

			echo '<option value="' . $item->ID . '" ' . $selected . '>' . $item->post_title . '</option>';
		}

		echo '</select>';
	}

	public function page_for_members_callback()
	{
		$members_page_id = get_option('page_for_members');

		$items = get_posts(array(
			'posts_per_page' => -1,
			'orderby'        => 'name',
			'order'          => 'ASC',
			'post_type'      => 'page',
		));

		echo '<select id="page_for_members" name="page_for_members">';
		echo '<option value="0">' . __('— Select —', 'wordpress') . '</option>';

		foreach ($items as $item) {
			$selected = ($members_page_id == $item->ID) ? 'selected="selected"' : '';

			echo '<option value="' . $item->ID . '" ' . $selected . '>' . $item->post_title . '</option>';
		}

		echo '</select>';
	}

	public function display_post_states($post_states, $post)
	{
		$resources_page_id = get_option('page_for_resources');
		$members_page_id   = get_option('page_for_members');

		if ($post->ID == $resources_page_id) {
			$post_states[] = __('Resources Page', 'egap');
		}

		if ($post->ID == $members_page_id) {
			$post_states[] = __('Members Page', 'egap');
		}

		return $post_states;
	}

	public function manage_term_custom_column($content, $column_name, $term_id)
	{
		if ($column_name === 'page-link') {
			$term = get_term($term_id);

			if ($term instanceof \WP_Term) {
				$tax       = $term->taxonomy;
				$post_type = ! empty($_REQUEST['post_type']) ? sanitize_text_field($_REQUEST['post_type']) : 'resource';
				$page_id   = $resources_page_id = get_option('page_for_' . $post_type . 's');
				$page_url  = add_query_arg($tax, $term->term_id, get_permalink($page_id));

				return '<a href="' . esc_attr($page_url) . '" target="_blank" rel="noopener noreferrer">' . esc_html($page_url) . ' <span class="screen-reader-text">(opens in a new tab)</span><span aria-hidden="true" class="dashicons dashicons-external"></span></a>';
			}
		}

		return $content;
	}

	public function manage_edit_term_custom_columns($columns)
	{
		$columns['page-link'] = 'Link';

		return $columns;
	}
}
