<?php

namespace Chisel\Extensions;

use Chisel\Helpers;
use Chisel\Media;
use Timber\Timber;

/**
 * Class Chisel
 * Chisel specific twig extensions. This class should not be changed during development.
 * @package Chisel\Extensions
 */
class ChiselTwig extends Twig
{
	private $manifest = array();

	public function extend()
	{
		add_filter('timber/loader/twig', array($this, 'extendTwig'));
	}

	/**
	 * Extends Twig, registers filters and functions.
	 *
	 * @param \Twig_Environment $twig
	 *
	 * @return \Twig_Environment $twig
	 */
	public function extendTwig($twig)
	{
		$this->registerFunction(
			$twig,
			'revisionedPath'
		);

		$this->registerFunction(
			$twig,
			'assetPath'
		);

		$this->registerFunction(
			$twig,
			'className'
		);

		$this->registerFunction(
			$twig,
			'ChiselPost',
			array(
				$this,
				'chiselPost',
			)
		);

		$this->registerFunction(
			$twig,
			'hasVendor'
		);

		$this->registerFunction(
			$twig,
			'getScriptsPath'
		);

		$this->registerFunction(
			$twig,
			'hasWebpackManifest'
		);

		$this->registerFunction(
			$twig,
			'getWebpackManifest'
		);

		$this->registerFunction(
			$twig,
			'tag'
		);

		$this->registerFunction(
			$twig,
			'imgTag'
		);

		$this->registerFunction(
			$twig,
			'svg'
		);

		$this->registerFunction(
			$twig,
			'src'
		);

		$this->registerFunction(
			$twig,
			'srcset'
		);

		$this->registerFunction(
			$twig,
			'sizes'
		);

		$this->registerFunction(
			$twig,
			'is_external_url'
		);

		$this->registerFunction(
			$twig,
			'render_overlay'
		);

		return $twig;
	}

	/**
	 * Get parsed manifest file content
	 *
	 * @return array
	 */
	public function getManifest()
	{
		if (empty($this->manifest)) {
			$this->initManifest();
		}

		return $this->manifest;
	}

	/**
	 * Returns the real path of the revisioned file.
	 * When CHISEL_DEV_ENV is defined it returns
	 *  path based on the manifest file content.
	 *
	 * @param $asset
	 *
	 * @return string
	 */
	public function revisionedPath($asset)
	{
		$pathinfo = pathinfo($asset);

		if ( ! defined('CHISEL_DEV_ENV')) {
			$manifest = $this->getManifest();
			if ( ! array_key_exists($pathinfo['basename'], $manifest)) {
				return 'FILE-NOT-REVISIONED';
			}

			return sprintf(
				'%s/%s%s/%s',
				get_template_directory_uri(),
				\Chisel\Settings::DIST_PATH,
				$pathinfo['dirname'],
				$manifest[$pathinfo['basename']]
			);
		} else {
			return sprintf(
				'%s/%s%s%s',
				get_template_directory_uri(),
				\Chisel\Settings::DIST_PATH,
				trim($asset, '/'),
				'?' . time()
			);
		}
	}

	/**
	 * Returns the real path of the asset file.
	 *
	 * @param $asset
	 *
	 * @return string
	 */
	public static function assetPath($asset)
	{
		return sprintf(
			'%s/%s%s',
			get_template_directory_uri(),
			\Chisel\Settings::ASSETS_PATH,
			trim($asset, '/')
		);
	}

	/**
	 * Builds class string based on name and modifiers
	 *
	 * @param string $name base class name
	 * @param string[] $modifiers,... class name modifiers
	 *
	 * @return string                built class
	 */
	public function className($name = '', $modifiers = null)
	{
		if ( ! is_string($name) || empty($name)) {
			return '';
		}
		$modifiers = array_slice(func_get_args(), 1);
		$classes   = array($name);
		foreach ($modifiers as $modifier) {
			if (is_string($modifier) && ! empty($modifier)) {
				$classes[] = $name . '--' . $modifier;
			}
		}

		return implode(' ', $classes);
	}

	/**
	 * Creates post with passed properties or loads default post when properties are missing
	 *
	 * @param array|null $fields
	 *
	 * @return \Chisel\Post
	 */
	public function chiselPost($fields = null)
	{
		return new \Chisel\Post($fields);
	}

	/**
	 * Verifies existence of the vendor.js file
	 *
	 * @return bool
	 */
	public function hasVendor()
	{
		if (defined('CHISEL_DEV_ENV')) {
			return file_exists(
				sprintf(
					'%s/%s%s',
					get_template_directory(),
					\Chisel\Settings::DIST_PATH,
					'scripts/vendor.js'
				)
			);
		} else {
			$manifest = $this->getManifest();

			return array_key_exists('vendor.js', $manifest);
		}
	}

	/**
	 * Returns the real path of the scripts directory.
	 *
	 * @return string
	 */
	public function getScriptsPath()
	{
		return sprintf(
			'%s/%s',
			get_template_directory_uri(),
			\Chisel\Settings::SCRIPTS_PATH
		);
	}

	/**
	 * Verifies existence of webpack manifest file.
	 *
	 * @return bool
	 */
	public function hasWebpackManifest()
	{
		return file_exists(
			sprintf(
				'%s/%s',
				get_template_directory(),
				\Chisel\Settings::getWebpackManifestPath()
			)
		);
	}

	/**
	 * Returns the contents of the webpack manifest file.
	 *
	 * @return string
	 */
	public function getWebpackManifest()
	{
		if ($this->hasWebpackManifest()) {
			return file_get_contents(
				sprintf(
					'%s/%s',
					get_template_directory(),
					\Chisel\Settings::getWebpackManifestPath()
				)
			);
		}

		return '';
	}

	/**
	 * Loads data from manifest file.
	 */
	private function initManifest()
	{
		if (file_exists(get_template_directory() . '/' . \Chisel\Settings::MANIFEST_PATH)) {
			$this->manifest = json_decode(
				file_get_contents(get_template_directory() . '/' . \Chisel\Settings::MANIFEST_PATH),
				true
			);
		}
	}

	public function tag($content, $class = '', $tag = 'div', array $attributes = array())
	{
		$content = trim($content);

		if ( ! $content && empty($attributes['force'])) {
			return '';
		}

		if (isset($attributes['force'])) {
			unset($attributes['force']);
		}

		if (isset($attributes['anim'])) {
			$class .= ' js-anim';
			unset($attributes['anim']);
		}

		if ($class) {
			$attributes['class'] = $class;
		}

		if (isset($attributes['orphans'])) {
			unset($attributes['orphans']);
			$content = apply_filters('iworks_orphan_replace', $content);
		}

		if (isset($attributes['striptags'])) {
			unset($attributes['striptags']);
			$content = strip_tags($content);
		}

		$attributes_string = array();

		foreach ($attributes as $attr => $attrData) {
			$attributes_string[] = $attr . '="' . esc_attr(trim($attrData)) . '"';
		}

		return '<' . $tag . ($attributes_string ? ' ' . implode(' ',
					$attributes_string) : '') . '>' . $content . '</' . $tag . '>';
	}

	public function imgTag($image_id, $size = 'full', $options = array())
	{
		if ( ! $image_id) {
			return '';
		}

		$pixel = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';

		if ($size == 'original') {
			$isResponsive = false;
			$size         = 'full';
		} else {
			$isResponsive = true;
		}

		$rest_request = defined('REST_REQUEST') && REST_REQUEST;

		$lazy = isset($options['lazy']) && !$rest_request ? $options['lazy'] : false;

		if ($lazy) {
			$options['class'] = ! empty($options['class']) ? $options['class'] . ' js-lazy-img' : 'js-lazy-img';
		}

		$attributes = '';
		$class      = isset($options['class']) ? ' class="' . esc_attr($options['class']) . '"' : '';
		$width      = isset($options['width']) ? ' width="' . esc_attr($options['width']) . '"' : '';
		$height     = isset($options['height']) ? ' height="' . esc_attr($options['height']) . '"' : '';
		$alt        = isset($options['alt']) ? $options['alt'] : '';
		$sizesAttr  = isset($options['sizes']) ? $options['sizes'] : '';
		$anim       = isset($options['anim']) ? ' ' . $options['anim'] : '';
		$src        = isset($options['src']) ? $options['src'] : '';
		$srcset     = isset($options['srcset']) ? ' ' . $options['srcset'] : '';

		$sizes = '';

		if ( ! empty($options['attributes'])) {
			if (is_string($options['attributes'])) {
				$attributes = ' ' . esc_attr(trim($options['attributes']));
			} elseif (is_array($options['attributes'])) {
				$attributes = '';
				foreach ($options['attributes'] as $attr_key => $attr_value) {
					$attributes .= ' ' . $attr_key . '="' . esc_attr($attr_value) . '"';
				}
			}
		}

		if ( ! is_numeric($image_id)) {
			return '<img' . $class . ($lazy ? ' src="' . $pixel . '" data-src' : ' src') . '="' . esc_attr($image_id) . '" alt="' . esc_attr($alt) . '"' . $attributes . $width . $height . $anim . '>';
		}


		if ( ! $src) {
			$image_data = wp_get_attachment_image_src($image_id, $size);

			if ( ! $image_data) {
				return '';
			}

			if (stripos($image_data[0], '.svg') || stripos($image_data[0], '.gif')) {
				$isResponsive = false;
			}

			if ( ! $alt) {
				$alt = get_post_meta($image_id, '_wp_attachment_image_alt', true);
			}

			if (stripos($image_data[0], '.gif') !== false && $size !== 'thumbnail') {
				$image_data = wp_get_attachment_image_src($image_id, 'full');
			}

			$src = $image_data[0];

			$path = ABSPATH . ltrim(str_ireplace(site_url(), '', $image_data[0]), '/');

			if ( ! $width && ! $height) {
				if (stripos($image_data[0], '.svg') && file_exists($path)) {
					$code = file_get_contents($path);
					preg_match('/<svg[^>]*width="?([0-9]+)"?[^>]*>/is', $code, $w);
					preg_match('/<svg[^>]*height="?([0-9]+)"?[^>]*>/is', $code, $h);

					if ($w) {
						$width = ' width="' . $w[1] . '"';
					}

					if ($h) {
						$height = ' height="' . $h[1] . '"';
					}
				} elseif ( ! empty($options['real-height'])) {
					$path = str_ireplace(site_url(), '', $image_data[0]);

					if (file_exists($path)) {
						$size = getimagesize(ltrim($path, '/'));
					}

					if ($size) {
						$width  = ' width="' . $size[0] . '"';
						$height = ' height="' . $size[1] . '"';
					}
				} else {
					if (empty($options['width'])) {
						$width = ' width="' . $image_data[1] . '"';
					}

					if (empty($options['height'])) {
						$height = ' height="' . $image_data[2] . '"';
					}
				}
			}
		}

		if ($isResponsive) {
			$srcset = ($lazy ? ' data-srcset' : ' srcset') . '="' . (isset($options['srcset']) ? $options['srcset'] : wp_get_attachment_image_srcset($image_id,
					$size)) . '"';

			$sizes = ($lazy ? ' data-sizes' : ' sizes') . '="' . (isset(Media::$sizesAttr[$sizesAttr]) ? Media::$sizesAttr[$sizesAttr] : wp_get_attachment_image_sizes($image_id,
					'full')) . '"';
		}

		return '<img' . $class . ($lazy ? ' src="' . $pixel . '" data-src' : ' src') . '="' . esc_attr($src) . '"' . $width . $height . ' alt="' . esc_attr($alt) . '"' . $attributes . $srcset . $sizes . '>';
	}

	public function svg($id, $class = '', array $attributes = array())
	{
		if ($class) {
			$attributes['class'] = $class;
		}

		$attributes['xmlns'] = 'http://www.w3.org/2000/svg';

		foreach ($attributes as $attr => $attrData) {
			$attributes_string[] = $attr . '="' . esc_attr($attrData) . '"';
		}

		$attributes_string = implode(' ', $attributes_string);

		if ($attributes_string) {
			$attributes_string = ' ' . $attributes_string;
		}

		return '<svg' . $attributes_string . '><use xlink:href="#' . $id . '"></use></svg>';
	}

	public function src($image_id, $size = 'full')
	{
		$image_src = wp_get_attachment_image_src($image_id, $size);
		if ( ! $image_src) {
			return '';
		}

		return $image_src[0];
	}

	public function srcset($image_id, $size = 'full')
	{
		$image_data = wp_get_attachment_image_src($image_id, $size);
		if ($image_data && (stripos($image_data[0], '.svg') || stripos($image_data[0], '.gif'))) {
			if (stripos($image_data[0], '.gif')) {
				return $image_data[0] . ' ' . $image_data[1] . 'w';
			} else {
				return $image_data[0];
			}
		}

		return wp_get_attachment_image_srcset($image_id, $size);
	}

	public function sizes($size = 'full')
	{
		if (isset(Media::$sizesAttr[$size])) {
			return Media::$sizesAttr[$size];
		}

		return '100vw';
	}

	public function is_external_url($url)
	{
		$site_url = str_replace(array('http:', 'https:'), '', site_url());
		$result = $url && $url[0] !== '#' && (stripos($url, $site_url) === false || preg_match('/(\.pdf|\.doc|\.docx|\.xls|\.xlsx)/i', $url) > 0);
		return $result;
	}

	public function render_overlay()
	{
		if (Helpers::$hasOverlay) {
			return Timber::compile('components/overlay.twig');
		}

		return '';
	}
}
