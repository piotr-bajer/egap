<?php

namespace Chisel\Extensions;

/**
 * Class Theme
 * Use this class to extend theme functionality
 * @package Chisel\Extensions
 */
class Theme implements ChiselExtension
{
	public function extend()
	{
		$this->addThemeSupports();
	}

	private function addThemeSupports()
	{
		add_theme_support('post-formats');
		add_theme_support('post-thumbnails');
		add_theme_support('menus');
		add_theme_support('title-tag');
		add_theme_support('align-wide');
		add_theme_support( 'wp-block-styles' );
		add_theme_support('editor-color-palette', array(
			array(
				'name'  => __('Black'),
				'slug'  => 'black',
				'color' => '#000000',
			),
			array(
				'name'  => __('White'),
				'slug'  => 'white',
				'color' => '#ffffff',
			),
			array(
				'name'  => __('Orange'),
				'slug'  => 'orange',
				'color' => '#ff5900',
			),
			array(
				'name'  => __('Gray'),
				'slug'  => 'gray',
				'color' => '#5f5e5e',
			),
		));

		add_theme_support('editor-styles');

		add_theme_support('editor-font-sizes', array(
			array(
				'name' => __('H1'),
				'size' => 50,
				'slug' => 'h1'
			),
			array(
				'name' => __('H2'),
				'size' => 50,
				'slug' => 'h2'
			),
			array(
				'name' => __('H3'),
				'size' => 30,
				'slug' => 'h3'
			),
			array(
				'name' => __('H4'),
				'size' => 28,
				'slug' => 'h4'
			),
			array(
				'name' => __('H5'),
				'size' => 23,
				'slug' => 'h5'
			),
			array(
				'name' => __('H6'),
				'size' => 18,
				'slug' => 'h6'
			),
			array(
				'name' => __('Small'),
				'size' => 14,
				'slug' => 'small'
			),
		));

		add_filter('excerpt_length', array($this, 'excerpt_length'), 100, 0);
		add_filter('terms_clauses', array($this, 'terms_clauses'), 99999, 3);
	}

	public function excerpt_length()
	{
		return 100;
	}

	public function terms_clauses($clauses, $taxonomy, $args)
	{
		global $wpdb;

		if ( ! empty($args['post_types'])) {
			$post_types = $args['post_types'];

			if (is_array($args['post_types'])) {
				$post_types = implode("','", $args['post_types']);
			}

			$clauses['join']  .= " INNER JOIN $wpdb->term_relationships AS r ON r.term_taxonomy_id = tt.term_taxonomy_id INNER JOIN $wpdb->posts AS p ON p.ID = r.object_id";
			$clauses['where'] .= " AND p.post_type IN ('" . esc_sql($post_types) . "') GROUP BY t.term_id";
		}

		return $clauses;
	}
}
