<?php

namespace Chisel\Extensions;

use Chisel\Helpers;
use Timber\Helper;

class Ajax implements ChiselExtension
{
	public function extend()
	{
		add_action('wp_ajax_tiles_data', array($this, 'tiles_data'));
		add_action('wp_ajax_nopriv_tiles_data', array($this, 'tiles_data'));
		add_action('wp_ajax_staff_data', array($this, 'staff_data'));
		add_action('wp_ajax_nopriv_staff_data', array($this, 'staff_data'));
		add_action('wp_ajax_member_data', array($this, 'member_data'));
		add_action('wp_ajax_nopriv_member_data', array($this, 'member_data'));
		add_action('wp_ajax_design_registry', array($this, 'design_registry'));
		add_action('wp_ajax_nopriv_design_registry', array($this, 'design_registry'));
	}

	public function tiles_data()
	{
		$offset         = ! empty($_REQUEST['offset']) ? (int)$_REQUEST['offset'] : 0;
		$post_types     = array('member', 'resource', 'staff');
		$post_type      = ! empty($_REQUEST['post_type']) && in_array($_REQUEST['post_type'],
			$post_types) ? $_REQUEST['post_type'] : 'resource';
		$search         = ! empty($_REQUEST['search']) ? sanitize_text_field($_REQUEST['search']) : '';
		$token          = ! empty($_REQUEST['token']) ? $_REQUEST['token'] : '';
		$posts_per_page = ! empty($_REQUEST['posts_per_page']) ? (int)$_REQUEST['posts_per_page'] : 3;
		array_walk_recursive($filters, 'intval');

		$parameters = array(
			'offset'         => $offset,
			'search'         => $search,
			'posts_per_page' => $posts_per_page,
		);

		$html = '';

		if ($post_type === 'member') {
			$data = Helpers::getMembersData($parameters);
		} elseif ($post_type === 'staff') {
			$data = Helpers::getStaffData($parameters);
		} else {
			$data = Helpers::getResourcesData($parameters);
		}

		foreach ($data['posts'] as $post) {
			$html .= \Timber\Timber::compile('partials/' . $post_type . '-item.twig',
				array_merge($post, array('class' => 'is-active')));
		}

		wp_send_json_success(array(
			'offset' => $offset,
			'html'   => $html,
			'token'  => $token,
			'count'  => count($data['posts']),
			'total'  => $data['total'],
		));
	}

	public function member_data()
	{
		$id   = ! empty($_REQUEST['id']) ? (int)$_REQUEST['id'] : 0;
		$data = \Chisel\Helpers::getMemberData($id);

		if ($data === false) {
			wp_send_json_error();
		}

		$html = \Timber\Timber::compile('components/member.twig',
			array_merge($data, array('class' => 'is-hidden')));

		wp_send_json_success(array(
			'html' => $html,
			'id'   => $id,
		));
	}

	public function staff_data()
	{
		$id   = ! empty($_REQUEST['id']) ? (int)$_REQUEST['id'] : 0;
		$data = \Chisel\Helpers::getStaffMemberData($id);

		if ($data === false) {
			wp_send_json_error();
		}

		$html = \Timber\Timber::compile('components/staff.twig',
			array_merge($data, array('class' => 'is-hidden')));

		wp_send_json_success(array(
			'html' => $html,
			'id'   => $id,
		));
	}

	public function design_registry()
	{
		$html = \Timber\Timber::compile('partials/design-registry-items.twig',
			\Chisel\Helpers::getDesignRegistryData());

		wp_send_json_success(array(
			'token' => ! empty($_REQUEST['token']) ? sanitize_text_field($_REQUEST['token']) : '',
			'html'  => $html,
		));
	}
}
