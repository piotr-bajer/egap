<?php

namespace Chisel\Extensions;

/**
 * Class DataType
 * Use this class to register custom post types and taxonomies
 * @package Chisel\Extensions
 */
class DataType implements ChiselExtension
{
	public function extend()
	{
		add_action('init', array($this, 'registerPostTypes'));
		add_action('init', array($this, 'registerTaxonomies'));

		if (function_exists('acf_add_options_page')) {
			acf_add_options_page(array(
				'page_title' => 'Theme Options',
				'menu_title' => 'Theme Options',
				'menu_slug'  => 'theme-options',
				'capability' => 'edit_posts',
				'redirect'   => false
			));
		}
	}

	/**
	 * Use this method to register custom post types
	 */
	public function registerPostTypes()
	{
		$resource_template = array(
			array(
				'egap/section',
				array(),
				array(
					array(
						'egap/section-column',
						array(),
						array(
							array('egap/breadcrumbs')
						)
					),
					array(
						'egap/section-columns',
						array('className' => 'is-resource-row is-header-row'),
						array(
							array(
								'core/column',
								array('verticalAlignment' => 'bottom'),
								array(
									array('egap/page-title', array('className' => 'is-style-smaller'))
								)
							),
							array(
								'core/column',
								array('verticalAlignment' => 'bottom'),
								array(
									array('core/paragraph')
								)
							),
						)
					),
					array(
						'egap/section-columns',
						array(),
						array(
							array(
								'core/column',
								array(),
								array(
									array('core/paragraph')
								)
							),
							array(
								'core/column',
								array(),
								array(
									array('egap/page-date'),
									array(
										'core/paragraph',
										array('placeholder' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquet augue felis, vitae placerat sapien mollis a. Duis consequat semper orci, ac consequat risus porttitor a. Integer vel vehicula augue, eget convallis diam. Quisque finibus tempor porta. Pellentesque bibendum dapibus magna, vel vehicula ligula maximus nec. Quisque commodo mi nibh, ac convallis nunc varius sodales. Nullam elementum mi elit. Maecenas sit amet laoreet velit. Integer laoreet finibus congue. Mauris vel urna ac ipsum bibendum tempus vitae in lorem. Suspendisse in pulvinar elit, a luctus sem. Sed quis magna ut ligula euismod vehicula sed ac purus. Suspendisse gravida erat eros, ac pretium arcu lacinia quis. Ut consectetur eu velit eu fermentum. Pellentesque porta dictum metus, ut fermentum augue varius in. Ut id lectus sit amet mauris hendrerit suscipit et sed felis.')
									),
								)
							),
						)
					),
				)
			),
		);

		foreach (
			array(
				'member'       => array(
					'singular'            => __('Member'),
					'plural'              => __('Members'),
					'publicly_queryable'  => true,
					'exclude_from_search' => true,
					'rewrite'             => array('slug' => 'member', 'with_front' => true),
					'menu_position'       => 4,
					'has_archive'         => false,
					'supports'            => array('title', 'revisions', 'thumbnail', 'editor'),
					'show_in_rest'        => true,
					'menu_icon'           => 'dashicons-admin-users',
				),
				'staff'        => array(
					'singular'            => __('Staff Member'),
					'plural'              => __('Staff Members'),
					'publicly_queryable'  => true,
					'exclude_from_search' => true,
					'rewrite'             => array('slug' => 'staff', 'with_front' => true),
					'menu_position'       => 4,
					'has_archive'         => false,
					'supports'            => array('title', 'revisions', 'thumbnail', 'editor'),
					'show_in_rest'        => true,
					'menu_icon'           => 'dashicons-admin-users',
				),
				'resource'     => array(
					'singular'            => __('Resource'),
					'plural'              => __('Resources'),
					'publicly_queryable'  => true,
					'exclude_from_search' => false,
					'rewrite'             => array('slug' => 'resource', 'with_front' => true),
					'menu_position'       => 4,
					'has_archive'         => false,
					'supports'            => array('title', 'revisions', 'thumbnail', 'editor'),
					'show_in_rest'        => true,
					'template'            => $resource_template,
					'menu_icon'           => 'dashicons-text-page',
				),
				'project'      => array(
					'singular'            => __('Project'),
					'plural'              => __('Projects'),
					'publicly_queryable'  => true,
					'exclude_from_search' => false,
					'rewrite'             => array('slug' => 'project', 'with_front' => true),
					'menu_position'       => 4,
					'has_archive'         => false,
					'supports'            => array('title', 'revisions', 'thumbnail', 'editor'),
					'show_in_rest'        => true,
					'template'            => $resource_template,
					'menu_icon'           => 'dashicons-admin-site-alt',
				),
				'registration' => array(
					'singular'            => __('Registration'),
					'plural'              => __('Registrations'),
					'publicly_queryable'  => false,
					'show_ui'             => false,
					'exclude_from_search' => true,
					'has_archive'         => false,
					'supports'            => array('title'),
					'show_in_rest'        => true,
				),
			) as $id => $data
		) {
			$this->addPostType($id, $data);
		}
	}

	/**
	 * Use this method to register custom taxonomies
	 */
	public function registerTaxonomies()
	{
		foreach (
			array(
				'member-type'          => array(
					'singular'           => __('Type'),
					'plural'             => __('Types'),
					'rewrite'            => array('slug' => 'member-type', 'with_front' => true),
					'posts'              => array('member'),
					'hierarchical'       => false,
					'show_ui'            => true,
					'show_in_rest'       => true,
					'publicly_queryable' => false,
				),
				'staff-position'       => array(
					'singular'           => __('Position'),
					'plural'             => __('Positions'),
					'rewrite'            => array('slug' => 'staff-position', 'with_front' => true),
					'posts'              => array('staff'),
					'hierarchical'       => false,
					'show_ui'            => true,
					'show_in_rest'       => true,
					'publicly_queryable' => false,
				),
				'thematic-area'        => array(
					'singular'           => __('Thematic Area'),
					'plural'             => __('Thematic Areas'),
					'rewrite'            => array('slug' => 'thematic-area', 'with_front' => true),
					'posts'              => array('resource'),
					'hierarchical'       => false,
					'show_ui'            => true,
					'show_in_rest'       => true,
					'publicly_queryable' => false,
				),
				'member-thematic-area' => array(
					'singular'           => __('Thematic Area'),
					'plural'             => __('Thematic Areas'),
					'rewrite'            => array('slug' => 'member-thematic-area', 'with_front' => true),
					'posts'              => array('member'),
					'hierarchical'       => false,
					'show_ui'            => true,
					'show_in_rest'       => true,
					'publicly_queryable' => false,
				),
				'research-region'      => array(
					'singular'           => __('Research Region'),
					'plural'             => __('Research Regions'),
					'rewrite'            => array('slug' => 'research-region', 'with_front' => true),
					'posts'              => array('member', 'resource'),
					'hierarchical'       => false,
					'show_ui'            => true,
					'show_in_rest'       => true,
					'publicly_queryable' => false,
				),
				'member-location'      => array(
					'singular'           => __('Member Location'),
					'plural'             => __('Member Locations'),
					'rewrite'            => array('slug' => 'member-location', 'with_front' => true),
					'posts'              => array('member'),
					'hierarchical'       => false,
					'show_ui'            => true,
					'show_in_rest'       => true,
					'publicly_queryable' => false,
				),
				'resource-type'        => array(
					'singular'           => __('Type'),
					'plural'             => __('Types'),
					'rewrite'            => array('slug' => 'resource-type', 'with_front' => true),
					'posts'              => array('resource'),
					'hierarchical'       => false,
					'show_ui'            => true,
					'show_in_rest'       => true,
					'publicly_queryable' => false,
				),
				'country'              => array(
					'singular'           => __('Country'),
					'plural'             => __('Countries'),
					'rewrite'            => array('slug' => 'country', 'with_front' => true),
					'posts'              => array('project', 'resource'),
					'hierarchical'       => false,
					'show_ui'            => true,
					'show_in_rest'       => true,
					'publicly_queryable' => false,
				),
			) as $id => $data
		) {
			$this->addTaxonomy($id, $data);
		}
	}

	private function addPostType($post_type, array $settings = array())
	{
		$defaults = array(
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array('slug' => $post_type, 'with_front' => false),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => 15,
			'can_export'         => true,
			'show_in_nav_menus'  => true,
			'taxonomies'         => array(),
			'supports'           => array('title', 'author', 'thumbnail', 'excerpt', 'comments'),
		);

		$args = wp_parse_args($settings, $defaults);

		if ( ! empty($settings['plural'])) {
			$args['labels'] = array(
				'name'              => __(esc_attr("{$settings['plural']}"), 'post type general name'),
				'singular_name'     => __(esc_attr("{$settings['singular']}"), 'post type singular name'),
				'parent_item_colon' => '',
				'menu_name'         => $settings['plural'],
			);
		}

		register_post_type($post_type, $args);
	}

	private function addTaxonomy($taxonomy, array $settings = array())
	{
		$defaults = array(
			'public'            => true,
			'hierarchical'      => true,
			'query_var'         => true,
			'show_admin_column' => true,
			'sort'              => false,
		);

		$args = wp_parse_args($settings, $defaults);

		if ( ! empty($settings['plural'])) {

			$labels = array(
				'name'                       => $settings['plural'],
				'singular_name'              => $settings['singular'],
				'menu_name'                  => isset($settings['menu_name']) ? $settings['menu_name'] : $settings['plural'],
				'search_items'               => 'Search ' . $settings['plural'],
				'all_items'                  => 'All ' . $settings['plural'],
				'edit_item'                  => 'Edit ' . $settings['singular'],
				'update_item'                => 'Update ' . $settings['singular'],
				'add_new_item'               => 'Add new ' . $settings['singular'],
				'new_item_name'              => 'New ' . $settings['singular'],
				'parent_item'                => 'Parent ' . $settings['singular'],
				'parent_item_colon'          => 'Parent ' . $settings['singular'],
				'separate_items_with_commas' => 'Separate tags with commas ' . $settings['plural'],
				'add_or_remove_items'        => 'Add or remove ' . $settings['plural'],
				'choose_from_most_used'      => 'Choose from most used ' . $settings['plural'],
			);

			if (false === $args['hierarchical']) {
				$labels['popular_items'] = 'Popular ' . $settings['plural'];
				if ( ! isset($settings['update_count_callback'])) {
					$args['update_count_callback'] = '_update_post_term_count';
				}
			}

			$args['labels'] = $labels;
		}

		register_taxonomy($taxonomy, $settings['posts'], $args);
	}
}
