<?php

namespace Chisel\Extensions;

use Chisel\Registration;

/**
 * Class Cron
 * Use this class to register custom post types and taxonomies
 * @package Chisel\Extensions
 */
class Cron implements ChiselExtension
{
	public function extend()
	{
		add_filter('cron_schedules', function ($schedules) {
			$schedules['three_minutes'] = array(
				'interval' => 3 * 60,
				'display'  => esc_html__('Every Three Minutes'),
			);

			return $schedules;
		});
		add_action('registrations_cron_job', array($this, 'registrations_cron_job'));

		if ( ! wp_next_scheduled('registrations_cron_job')) {
			wp_schedule_event(time(), 'three_minutes', 'registrations_cron_job');
		}
	}

	private function run_for($callable, $time = 120)
	{
		$start_cron_time = get_transient( 'doing_cron' );
		$start_time = time();
		$cron_time_limit = 140;

		while(microtime( true ) - $start_cron_time < $cron_time_limit && time() - $start_time < $time){
			if(call_user_func($callable) === true) {
				break;
			}
		}
	}

	private function download_registration_data()
	{
		$registration_progress = (int)get_option('egap_registration_download_progress', 0);
		$registration_total    = (int)get_option('egap_registration_total', 0);
		$registration_revision = (int)get_option('egap_registration_revision', 0);

		if ($registration_total === 0) {
			$result = Registration::search(Registration::prepareQuery(array(
				'size' => 1,
			)));

			if ($result) {
				$registrations_count = (int)$result['hits']['total'];
				$posts_count         = wp_count_posts('registration');

				if ($registrations_count !== $posts_count->publish) {
					$registration_total = $registrations_count;
					update_option('egap_registration_total', $registration_total, false);
				}
			} else {
				update_option('egap_registration_download_status', 'idle', false);
				return true;
			}
		}

		$page_size = 200;
		$result    = Registration::search(Registration::prepareQuery(array(
			'size' => $page_size,
			'from' => $registration_progress,
		)));

		$hits_count = $result && ! empty($result['hits']['hits']) ? count($result['hits']['hits']) : 0;

		if ($hits_count > 0) {
			$hit_ids       = array();
			$registrations = array();

			foreach ($result['hits']['hits'] as $hit) {
				$registration   = Registration::parse_hit($hit);
				$hit_ids[]      = $registration['link_id'];
				$registrations[] = $registration;
			}

			global $wpdb;
			$query_result = $wpdb->get_results("select p.ID as post_id, p.post_name as registration_id
				from {$wpdb->posts} p
				where p.post_type = 'registration'
				and p.post_name in ('" . implode("','", $hit_ids) . "')", ARRAY_A);

			$post_data_by_registration_id = array();

			foreach ($query_result as $row) {
				$post_data_by_registration_id[$row['registration_id']] = $row['post_id'];
			}

			$post_inserts = array();

			foreach ($registrations as $registration) {
				if (isset($post_data_by_registration_id[$registration['link_id']])) {
					$wpdb->query(
						$wpdb->prepare("update {$wpdb->posts} p
								set p.post_content = %s, p.post_modified_gmt = %s, p.post_excerpt = %s
								where p.post_type = 'registration'
								and p.post_name = %s",
							json_encode($registration),
							date('Y-m-d H:i:s', $registration_revision),
							$registration['title'],
							$registration['link_id']
						));
				} else {
					$post_inserts[] = $wpdb->prepare('(%s, %s, %s, %s, %s, %s)',
						json_encode($registration),
						$registration['link_id'],
						'publish',
						'registration',
						date('Y-m-d H:i:s', $registration_revision),
						$registration['title']
					);
				}
			}

			if (count($post_inserts) > 0) {
				$wpdb->query("insert into {$wpdb->posts} (post_content, post_name, post_status, post_type, post_modified_gmt, post_excerpt) values " . implode(',',
						$post_inserts));
			}
		}

		$registration_progress = min($registration_progress + $page_size, $registration_total);

		if($registration_progress >= $registration_total) {
			update_option('egap_registration_total', 0, false);
			update_option('egap_registration_download_progress', 0, false);
			update_option('egap_registration_download_status', 'download_dates', false);
			return true;
		} else {
			update_option('egap_registration_download_progress', $registration_progress, false);
		}

		return false;
	}

	private function download_registration_dates()
	{
		global $wpdb;
		$limit = 20;
		$ids_to_download = $wpdb->get_col("select post_name from {$wpdb->posts}
							where post_type ='registration' and post_title = '' limit {$limit}");

		if(!$ids_to_download || count($ids_to_download) < 1) {
			update_option('egap_registration_download_status', 'remove_registrations', false);
			return true;
		}

		$result         = Registration::getDetails($ids_to_download, $limit);

		if ( $result ) {
			foreach ($result as $registration_id => $registration_data) {
				$query = $wpdb->prepare("update {$wpdb->posts} p
								set p.post_title = %s, p.post_date = %s, p.post_date_gmt = %s
								where p.post_type = 'registration'
								and p.post_name = %s",
					$registration_data['original_id'],
					$registration_data['original_date'],
					$registration_data['original_date_gmt'],
					$registration_id);
				$wpdb->query($query);
			}
		}

		return false;
	}

	public function remove_registrations() {
		$revision = (int)get_option('egap_registration_revision', time());

		if($revision > 0) {
			global $wpdb;
			$wpdb->query(
				$wpdb->prepare(
					"update {$wpdb->posts}
							set post_status = 'trash'
							where post_type = 'registration'
							and post_modified_gmt <> %s", date('Y-m-d H:i:s', $revision)
				));
		}

		update_option('egap_registration_download_status', 'idle', false);
		update_option('egap_last_registration_download', time(), false);

		return true;
	}

	public function registrations_cron_job()
	{
		$last_registration_download   = (int)get_option('egap_last_registration_download', 0);
		$registration_download_status = get_option('egap_registration_download_status', 'idle');

		if ($registration_download_status === 'idle'
		    && time() - $last_registration_download > 2 * HOUR_IN_SECONDS) {
			$registration_download_status = 'download_data';
			update_option('egap_registration_download_status', $registration_download_status, false);
			update_option('egap_registration_revision', time(), false);
			update_option('egap_registration_download_progress', 0, false);
			update_option('egap_registration_total', 0, false);
		}

		if ($registration_download_status === 'download_data') {
			$this->run_for(array($this, 'download_registration_data'));
		} elseif ($registration_download_status === 'download_dates') {
			$this->run_for(array($this, 'download_registration_dates'));
		} elseif ($registration_download_status === 'remove_registrations') {
			$this->run_for(array($this, 'remove_registrations'));
		}
	}
}
