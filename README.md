# EGAP

In order to review the code please investigate these folders:

- src/scripts
- src/styles
- wp/wp-content/plugins/egap-blocks/src
- wp/wp-content/themes/egap-chisel

EGAP is a project created with Chisel. Please check out Chisel documentation at [https://www.getchisel.co/docs/](www.getchisel.co/docs/).

