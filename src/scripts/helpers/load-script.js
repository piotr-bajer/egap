const loadedScripts = {};

export default (scriptSrc, callback) => {
  let scriptsToLoaded = [];
  let count = 0;

  if (typeof scriptSrc === 'string') {
    scriptsToLoaded = [scriptSrc];
  } else {
    scriptsToLoaded = scriptSrc;
  }

  const checkLoad = () => {
    count += 1;
    if (count === scriptsToLoaded.length) {
      callback();
    }
  };

  [].forEach.call(scriptsToLoaded, src => {
    if (loadedScripts[src] === true) {
      checkLoad();
    } else {
      const script = document.createElement('script');

      script.onload = () => {
        loadedScripts[src] = true;
        checkLoad();
      };

      script.src = src;
      document.body.appendChild(script);
    }
  });
};
