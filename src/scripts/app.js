/*
  Project: EGAP
  Author: Piotr Bajer
 */

import 'whatwg-fetch';
import 'url-search-params-polyfill';
import smoothscroll from 'smoothscroll-polyfill';
import fix100vh from './fixes/100vh';
import objectFit from './polyfills/object-fit';
import composedPath from './polyfills/composed-path';
import closest from './polyfills/closest';
import lazyLoad from './modules/lazy-load';
import header from './modules/header';
import expand from './modules/expand';
import filter from './modules/filter';
import members from './modules/members';
import relatedResources from './modules/related-resources';
import imageSlides from './modules/image-slides';
import hero from './modules/hero';
import links from './modules/links';
import newsletter from './modules/newsletter';
import designRegistry from './modules/design-registry';

smoothscroll.polyfill();
fix100vh();
objectFit();
composedPath();
closest();
lazyLoad();
header();
expand();
filter();
members();
relatedResources();
imageSlides();
hero();
links();
newsletter();
designRegistry();
