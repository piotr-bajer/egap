import { on, each } from '../helpers/shortcuts';
import lazyload from './lazy-load';
import S from './shared';

export default () => {
  const filterContainers = document.querySelectorAll('.c-filter');

  if (filterContainers.length < 1) {
    return;
  }

  let openedSelectInputs = [];

  const clearInputs = () => {
    each(openedSelectInputs, input => {
      input.classList.remove(S.CSS.active);
    });

    openedSelectInputs = [];
  };

  each(filterContainers, filterContainer => {
    const selectElements = filterContainer.querySelectorAll(
      '.c-filter__select',
    );
    const itemsContainer = filterContainer.querySelector('.c-filter__items');
    const moreButton = filterContainer.querySelector('.c-filter__more');
    const form = filterContainer.querySelector('.c-filter__form');
    const allButton = filterContainer.querySelector('.c-filter__all');
    const searchInput = filterContainer.querySelector(
      '.c-filter__search-input',
    );
    let filterValues = {};
    let initialFilterValues = {};
    let token = 0;
    let revealed = 0;
    let { total } = filterContainer.dataset;
    const isMembers = filterContainer.classList.contains('c-members');
    const memberType = filterContainer.classList.contains('c-members--staff')
      ? 'staff'
      : 'member';
    const moreButtonText = moreButton.textContent;
    let moreLoading = false;
    const url = window.APP_DATA.ajax_url;

    const postsPerPage = () => {
      if (S.windowWidth < S.breakpoints.medium) {
        return isMembers ? 6 : 3;
      }
      if (S.windowWidth < S.breakpoints.large) {
        return isMembers ? 12 : 6;
      }
      return isMembers ? 24 : 9;
    };

    const checkMore = () => {
      if (revealed < total) {
        filterContainer.classList.remove('no-more');
      } else {
        filterContainer.classList.add('no-more');
      }
    };

    const checkRevealed = () => {
      revealed = Math.min(
        itemsContainer.children.length,
        revealed === 0 ? postsPerPage() : revealed,
      );

      for (let i = 0; i < revealed; i += 1) {
        itemsContainer.children[i].classList.add(S.CSS.active);
      }
    };

    const loadData = (offset, replaceUrl = null, changeState = true) => {
      token = new Date().getTime();

      const parameters = {
        offset: offset === undefined ? 0 : offset,
        action: 'tiles_data',
        post_type: isMembers ? memberType : 'resource',
        search: searchInput.value,
        posts_per_page: postsPerPage(),
        token,
      };
      const filterNames = Object.keys(filterValues);
      const URLParts = [];

      for (let i = 0; i < filterNames.length; i += 1) {
        const key = filterNames[i];
        const values = filterValues[filterNames[i]];

        parameters[key] = values.join(',');

        if (values.length > 0) {
          URLParts.push(`${key}=${parameters[key]}`);
        }
      }

      const newUrl =
        replaceUrl !== null
          ? replaceUrl
          : `${window.location.href.split('?')[0]}?${URLParts.join('&')}`;

      if (parameters.offset === 0) {
        filterContainer.classList.add(S.CSS.loading);
      }

      const urlParameters = new URLSearchParams(parameters);

      window
        .fetch(url, {
          method: 'POST',
          body: urlParameters,
        })
        .then(response => response.json())
        .then(data => {
          if (parseInt(data.data.token, 10) === token) {
            if (parseInt(data.data.offset, 10) === 0) {
              itemsContainer.innerHTML = '';
              revealed = 0;

              if (changeState) {
                window.history.pushState(
                  {
                    url: newUrl,
                    search: parameters.search,
                    filterValues: JSON.parse(JSON.stringify(filterValues)),
                  },
                  document.title,
                  newUrl,
                );
              }
            }

            itemsContainer.insertAdjacentHTML('beforeend', data.data.html);
            lazyload().update();
            moreLoading = false;
            moreButton.textContent = moreButtonText;
            token = parseInt(data.data.token, 10);
            total = parseInt(data.data.total, 10);
            revealed += parseInt(data.data.count, 10);
            checkRevealed();
            checkMore();
            filterContainer.classList.remove(S.CSS.loading);
          }
        });
    };

    checkRevealed();
    checkMore();

    each(selectElements, selectElement => {
      const { taxonomy } = selectElement.dataset;
      filterValues[taxonomy] = [];
      const buttons = selectElement.querySelectorAll(
        '.c-filter__select-button',
      );

      on('click', selectElement, event => {
        event.stopPropagation();
      });

      on('click', selectElement.firstElementChild, () => {
        const isActive = selectElement.classList.contains(S.CSS.active);
        clearInputs();

        if (!isActive) {
          selectElement.classList.add(S.CSS.active);
          openedSelectInputs.push(selectElement);
        }
      });

      on('click', buttons, event => {
        const button = event.currentTarget;
        button.classList.toggle(S.CSS.active);
        const { id } = button.dataset;
        const active = button.classList.contains(S.CSS.active);
        const index = filterValues[taxonomy].indexOf(id);

        if (active && index < 0) {
          filterValues[taxonomy].push(id);
        } else if (!active && index > -1) {
          filterValues[taxonomy].splice(index, 1);
        }

        loadData();
      });

      each(buttons, button => {
        if (button.classList.contains(S.CSS.active)) {
          filterValues[taxonomy].push(button.dataset.id);
        }
      });
    });

    initialFilterValues = JSON.parse(JSON.stringify(filterValues));

    on('click', moreButton, event => {
      event.preventDefault();

      const newRevealed = Math.min(revealed + postsPerPage(), total);

      if (newRevealed <= itemsContainer.children.length) {
        revealed = newRevealed;
        checkRevealed();
        checkMore();
      } else if (!moreLoading) {
        const revealedDiff = Math.max(
          itemsContainer.children.length - revealed,
          0,
        );
        moreLoading = true;
        moreButton.textContent = 'Loading...';
        revealed += revealedDiff;
        loadData(revealed);
      }
    });

    on('resize', window, () => {
      checkRevealed();
      checkMore();
    });

    on('submit', form, event => {
      event.preventDefault();
      loadData();
    });

    on('click', allButton, event => {
      event.preventDefault();
      const keys = Object.keys(filterValues);

      for (let i = 0; i < keys.length; i += 1) {
        filterValues[keys[i]] = [];
      }

      searchInput.value = '';

      const buttons = filterContainer.querySelectorAll(
        '.c-filter__select-button',
      );

      each(buttons, button => {
        button.classList.remove(S.CSS.active);
      });

      loadData();
    });

    on('popstate', window, event => {
      const { state } = event;

      filterValues = JSON.parse(
        JSON.stringify(
          state === null ? initialFilterValues : state.filterValues,
        ),
      );

      each(form.querySelectorAll('.is-active'), el => {
        el.classList.remove('is-active');
      });

      if (state === null) {
        searchInput.value = '';
      } else {
        searchInput.value = state.search;

        each(selectElements, selectElement => {
          const { taxonomy } = selectElement.dataset;

          if (
            state.filterValues[taxonomy] !== undefined &&
            state.filterValues[taxonomy].length > 0
          ) {
            state.filterValues[taxonomy].forEach(id =>
              selectElement
                .querySelector(`[data-id="${id}"]`)
                .classList.add('is-active'),
            );
          }
        });
      }

      loadData(0, null, false);
    });
  });

  on('click', document.documentElement, clearInputs);
};
