import { on, each } from '../helpers/shortcuts';
import S from './shared';

export default () => {
  const imageSlides = document.querySelectorAll('.c-image-slides');

  if (imageSlides.length < 1) {
    return;
  }

  each(imageSlides, container => {
    const buttons = container.querySelectorAll('.o-filter-header__button');

    if (buttons.length > 0) {
      const itemsContainer = container.querySelector('.c-image-slides__items');

      each(buttons, (button, index) => {
        on('click', button, event => {
          event.preventDefault();
          each(buttons, b => b.classList.remove(S.CSS.active));
          each(itemsContainer.children, i => i.classList.remove(S.CSS.active));
          button.classList.add(S.CSS.active);
          itemsContainer.children[index].classList.add(S.CSS.active);
        });
      });
    }
  });
};
