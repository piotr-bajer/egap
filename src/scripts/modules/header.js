import { on, each } from '../helpers/shortcuts';
import S from './shared';

export default () => {
  const header = document.querySelector('.c-header');

  if (header === null) {
    return;
  }

  const hamburger = header.querySelector('.c-header__hamburger');
  const menu = header.querySelector('.c-header__menu');
  const submenus = header.querySelectorAll(
    '.menu-item-has-children.c-main-nav__item > .c-main-nav__link',
  );

  let activeSubmenus = [];

  const clearSubmenus = () => {
    each(activeSubmenus, submenu => submenu.classList.remove(S.CSS.active));
    activeSubmenus = [];
  };

  on('click', hamburger, event => {
    event.preventDefault();
    event.stopPropagation();

    hamburger.classList.toggle(S.CSS.active);
    menu.classList.toggle(S.CSS.active);
  });

  on('click', menu, event => {
    event.stopPropagation();
  });

  on('click', submenus, event => {
    event.stopPropagation();
    const submenu = event.currentTarget.parentNode;
    const isActive = submenu.classList.contains(S.CSS.active);

    if (!isActive && (S.windowWidth < S.breakpoints.xlarge || S.isTouch)) {
      event.preventDefault();
      clearSubmenus();
      submenu.classList.add(S.CSS.active);
      activeSubmenus.push(submenu);
    } else if (
      event.target.classList.contains('c-main-nav__chevron') &&
      isActive
    ) {
      event.preventDefault();
      clearSubmenus();
    }
  });

  on('click', document.documentElement, () => {
    if (S.windowWidth <= S.breakpoints.xlarge) {
      hamburger.classList.remove(S.CSS.active);
      menu.classList.remove(S.CSS.active);
    }

    clearSubmenus();
  });
};
