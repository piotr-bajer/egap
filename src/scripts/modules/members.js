import S from './shared';
import { on, nextFrame } from '../helpers/shortcuts';
import { lockViewport, unlockViewport } from '../helpers/viewport-lock';
import lazyload from './lazy-load';

export default () => {
  const overlay = document.querySelector('.c-overlay');

  if (overlay === null) {
    return;
  }

  const url = window.APP_DATA.ajax_url;
  const overlayInner = overlay.querySelector('.c-overlay__inner');
  const memberTriggers = document.querySelectorAll('[data-member-trigger]');
  const staffTriggers = document.querySelectorAll('[data-staff-trigger]');
  const member = document.querySelector('.c-member');
  const loadedMembers = {};
  let currentMember =
    member !== null && member.dataset.id !== undefined ? member.dataset.id : 0;

  if (currentMember !== 0) {
    loadedMembers[currentMember] = member;
  }

  let overlayTimeout = null;
  let hideTimeout = null;

  const showMember = id => {
    if (loadedMembers[id] !== undefined) {
      clearTimeout(hideTimeout);
      loadedMembers[id].classList.remove(S.CSS.hidden);
      overlayInner.style.minHeight = '';
    }
  };

  const hideMember = id => {
    if (loadedMembers[id] !== undefined) {
      clearTimeout(hideTimeout);
      loadedMembers[id].classList.add(S.CSS.hidden);
      overlayInner.style.minHeight = `${
        loadedMembers[id].getBoundingClientRect().height
      }px`;

      hideTimeout = setTimeout(() => {
        overlayInner.style.minHeight = '';
      }, 600);
    }
  };

  const showOverlay = () => {
    if (overlay.classList.contains(S.CSS.active)) {
      return;
    }

    overlay.classList.add(S.CSS.active);
    lockViewport(overlay);
    clearTimeout(overlayTimeout);
  };

  const hideOverlay = () => {
    if (!overlay.classList.contains(S.CSS.active)) {
      return;
    }

    overlay.classList.remove(S.CSS.active);
    unlockViewport(overlay);

    overlayTimeout = setTimeout(() => {
      hideMember(currentMember);
      currentMember = 0;
    }, 500);
  };

  const loadMember = (id, type = 'member') => {
    showOverlay();

    if (loadedMembers[id] === undefined) {
      overlay.classList.add(S.CSS.loading);
      window
        .fetch(url, {
          method: 'POST',
          body: new URLSearchParams({
            id,
            action: `${type}_data`,
          }),
        })
        .then(response => response.json())
        .then(data => {
          if (data.success) {
            overlayInner.insertAdjacentHTML('beforeend', data.data.html);
            lazyload().update();
            loadedMembers[data.data.id] = overlayInner.lastElementChild;

            if (currentMember.toString() === data.data.id.toString()) {
              nextFrame(() => {
                showMember(currentMember);
                overlay.classList.remove(S.CSS.loading);
              });
            }
          }
        });
    } else {
      hideMember(currentMember);
      showMember(id);
    }

    currentMember = id;
  };

  if (memberTriggers.length) {
    on('click', memberTriggers, event => {
      event.preventDefault();
      overlay.scrollTo({ top: 0 });
      loadMember(event.currentTarget.dataset.memberTrigger, 'member');
    });
  }

  if (staffTriggers.length) {
    on('click', memberTriggers, event => {
      event.preventDefault();
      overlay.scrollTo({ top: 0 });
      loadMember(event.currentTarget.dataset.memberTrigger, 'staff');
    });
  }

  on('click', overlay, event => {
    const path = event.composedPath();

    for (let i = 0; i < path.length; i += 1) {
      const el = path[i];

      if (el.classList) {
        if (
          el.classList.contains('c-members__item-website') ||
          el.classList.contains('c-overlay__inner')
        ) {
          event.stopPropagation();
          break;
        } else if (el.classList.contains('o-read-more')) {
          el.parentNode.parentNode.firstElementChild.classList.remove(
            'u-show-medium',
          );
          el.parentNode.parentNode.removeChild(el.parentNode);
          event.stopPropagation();
          break;
        } else if (el.classList.contains('c-member__view-all')) {
          event.stopPropagation();
          hideOverlay();
          break;
        } else if (el.dataset && el.dataset.overlayClose !== undefined) {
          event.preventDefault();
          event.stopPropagation();
          hideOverlay();
          break;
        } else if (
          el.dataset &&
          el.classList.contains('c-member__member') &&
          el.dataset.id !== undefined &&
          el.dataset.type !== undefined
        ) {
          event.stopPropagation();
          overlay.scrollTo({ top: 0, behavior: 'smooth' });
          hideMember(currentMember);
          loadMember(el.dataset.id, el.dataset.type);
          break;
        }
      }
    }
  });

  const memberContainers = document.querySelectorAll('.c-members');

  if (memberContainers.length > 0) {
    on('click', memberContainers, event => {
      const path = event.composedPath();

      for (let i = 0; i < path.length; i += 1) {
        const el = path[i];

        if (el.classList && el.classList.contains('c-members__item')) {
          event.stopPropagation();
          overlay.scrollTo({ top: 0 });
          loadMember(el.dataset.id);
          break;
        }
      }
    });
  }
};
