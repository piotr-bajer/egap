import { on, each } from '../helpers/shortcuts';

export default () => {
  const expandTargets = document.querySelectorAll('.o-expand__target');

  if (expandTargets.length < 1) {
    return;
  }

  const updateDimensions = () => {
    each(expandTargets, target => {
      const rect = target.parentNode.getBoundingClientRect();
      // eslint-disable-next-line no-param-reassign
      target.style.width = `${rect.width}px`;
      // eslint-disable-next-line no-param-reassign
      target.style.height = `${rect.height}px`;
    });
  };

  requestAnimationFrame(updateDimensions);

  let resizeTimeout = null;

  on('load resize', () => {
    clearTimeout(resizeTimeout);

    resizeTimeout = setTimeout(() => {
      requestAnimationFrame(updateDimensions);
    }, 100);
  });
};
