import { on, each } from '../helpers/shortcuts';
import S from './shared';

export default () => {
  const relatedResources = document.querySelectorAll('.c-related-resources');

  if (relatedResources.length < 1) {
    return;
  }

  each(relatedResources, container => {
    const filterButtons = container.querySelectorAll(
      '.o-filter-header__button[data-id]',
    );

    const filterButtonViewAll = container.querySelector(
      '.o-filter-header__button:not([data-id])',
    );

    if (filterButtons.length > 0) {
      const itemsContainer = container.querySelector(
        '.c-related-resources__items',
      );

      on('click', filterButtons, event => {
        event.preventDefault();
        each(filterButtons, button => button.classList.remove(S.CSS.active));
        event.currentTarget.classList.add(S.CSS.active);

        const { id } = event.currentTarget.dataset;

        each(itemsContainer.children, child =>
          child.classList.remove(S.CSS.hidden),
        );
        const itemsToHide = container.querySelectorAll(
          `.c-related-resources__items > *:not([data-id~="${id}"])`,
        );
        each(itemsToHide, item => item.classList.add(S.CSS.hidden));
      });

      on('click', filterButtonViewAll, event => {
        event.preventDefault();

        each(filterButtons, button => button.classList.remove(S.CSS.active));
        each(itemsContainer.children, child =>
          child.classList.remove(S.CSS.hidden),
        );
      });
    }
  });
};
