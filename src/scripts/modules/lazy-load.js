import LazyLoad from 'vanilla-lazyload';

let lazyLoad = null;

export default () => {
  if (lazyLoad === null) {
    const loaded = el => {
      el.classList.remove('js-lazy-loaded');
      el.classList.remove('js-lazy-img');
    };

    lazyLoad = new LazyLoad({
      load_delay: 300,
      elements_selector: '.js-lazy-img',
      class_loading: 'js-lazy-loading',
      class_loaded: 'js-lazy-loaded',
      class_error: 'js-lazy-loaded',
      callback_loaded: el => {
        setTimeout(() => {
          loaded(el);
        }, 300);
      },
    });
  }
  return lazyLoad;
};
