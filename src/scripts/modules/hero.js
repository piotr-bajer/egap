import { on } from '../helpers/shortcuts';

export default () => {
  const hero = document.querySelector('.c-hero');

  if (hero === null) {
    return;
  }

  const button = hero.querySelector('.c-hero__button');

  if (button !== null) {
    on('click', button, event => {
      event.preventDefault();
      hero.nextElementSibling.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
      });
    });
  }
};
