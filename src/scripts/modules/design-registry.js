import { on, createEvent } from '../helpers/shortcuts';
import S from './shared';

export default () => {
  const designRegistry = document.querySelector('.c-design-registry');

  if (designRegistry === null) {
    return;
  }

  const toggleButtons = designRegistry.querySelectorAll(
    '.c-design-registry__item-toggle',
  );
  const sort = designRegistry.querySelector('.c-design-registry__sort');
  const form = designRegistry.querySelector('.c-design-registry__form');
  const search = designRegistry.querySelector(
    '.c-design-registry__search-input',
  );
  const items = designRegistry.querySelector('.c-design-registry__items');
  const helpTrigger = designRegistry.querySelector(
    '.c-design-registry__help-trigger',
  );
  const help = designRegistry.querySelector('.c-design-registry__help');
  let token = 0;
  const currentPage = 0;
  // eslint-disable-next-line
  const loadData = () => {
    designRegistry.classList.add(S.CSS.loading);

    const url = window.APP_DATA.ajax_url;
    token = new Date().getTime();

    window
      .fetch(url, {
        method: 'POST',
        body: new URLSearchParams({
          post_id: window.APP_DATA.post_id,
          sort_by: sort.value,
          search: search.value,
          from: currentPage,
          size: 10,
          token,
          action: 'design_registry',
        }),
      })
      .then(response => response.json())
      .then(data => {
        if (
          typeof data === 'object' &&
          data.success &&
          data.data.token === token.toString()
        ) {
          items.innerHTML = '';
          items.insertAdjacentHTML('beforeend', data.data.html);
          designRegistry.classList.remove(S.CSS.loading);
        }
      });
  };

  on('click', toggleButtons, event => {
    event.preventDefault();

    event.currentTarget.parentNode.classList.toggle(S.CSS.active);
  });

  on('change', sort, () => {
    form.dispatchEvent(createEvent('submit'));
  });

  on('submit', form, () => {
    form.submit();
  });

  on('click', helpTrigger, event => {
    event.preventDefault();
    event.stopPropagation();
    helpTrigger.classList.toggle(S.CSS.active);
  });

  on('click', help, event => {
    event.stopPropagation();
  });

  on('click', document.body, () => {
    helpTrigger.classList.remove(S.CSS.active);
  });
};
