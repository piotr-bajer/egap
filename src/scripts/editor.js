/*
  Project: EGAP
  Author: Piotr Bajer
 */

import hideOn from './editor/hide-on';
import spacing from './editor/spacing';
import removeStylesBlock from './editor/remove-styles-block';
import nowrap from './editor/nowrap';

hideOn();
spacing();
removeStylesBlock();
nowrap();
