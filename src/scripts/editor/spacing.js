/* eslint-disable no-undef, no-unused-vars */
import classnames from 'classnames';

const { createHigherOrderComponent } = wp.compose;
const { Fragment } = wp.element;
const { InspectorControls } = wp.blockEditor;
const { SelectControl, PanelBody } = wp.components;
const { addFilter } = wp.hooks;
const { __ } = wp.i18n;

const margins = [
  {
    label: __('Default'),
    value: '',
  },
  {
    label: __('No Margin'),
    value: 'u-mb-0',
  },
  {
    label: __('Small'),
    value: 'u-mb-small',
  },
  {
    label: __('Medium'),
    value: 'u-mb-medium',
  },
  {
    label: __('Large'),
    value: 'u-mb-large',
  },
];

export default () => {
  const withSpacing = createHigherOrderComponent(
    BlockEdit => props => {
      let { className = '' } = props.attributes;
      const { setAttributes } = props;
      let marginBottom = className ? className.match(/u-mb-[a-z0-9]+/) : '';
      marginBottom = marginBottom ? marginBottom.pop() : '';

      return (
        <Fragment>
          <BlockEdit {...props} />
          <InspectorControls>
            <PanelBody title={__('Spacing')} initialOpen={false}>
              <SelectControl
                label={__('Margin Bottom')}
                value={marginBottom}
                options={margins}
                onChange={selectedMarginBottom => {
                  className =
                    className && className.replace(/ ?u-mb-[a-z0-9]+/g, '');
                  className = classnames(className, selectedMarginBottom);
                  setAttributes({
                    marginBottom: selectedMarginBottom,
                    className,
                  });
                }}
              />
            </PanelBody>
          </InspectorControls>
        </Fragment>
      );
    },
    'withSpacing',
  );

  addFilter('editor.BlockEdit', 'egap/with-spacing', withSpacing);
};
