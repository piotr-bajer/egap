/* eslint-disable no-undef, no-unused-vars */

const { registerFormatType, toggleFormat } = wp.richText;
const { RichTextToolbarButton } = wp.blockEditor;
const { Path: path, SVG: svg } = wp.components;

const icon = (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
    <path fill="none" d="M0 0h24v24H0V0z" />
    <path d="M18 9v4H6V9H4v6h16V9z" />
  </svg>
);

export default () => {
  const NowrapButton = props => (
    <RichTextToolbarButton
      icon={icon}
      title="Nowrap"
      onClick={() => {
        props.onChange(toggleFormat(props.value, { type: 'egap/nowrap' }));
      }}
      isActive={props.isActive}
    />
  );

  registerFormatType('egap/nowrap', {
    title: 'Nowrap',
    tagName: 'span',
    className: 'is-style-nowrap',
    edit: NowrapButton,
  });
};
