/* eslint-disable no-undef, no-unused-vars */

const { createHigherOrderComponent } = wp.compose;
const { Fragment } = wp.element;
const { InspectorControls } = wp.blockEditor;
const { PanelBody, CheckboxControl } = wp.components;
const { addFilter } = wp.hooks;
const { __ } = wp.i18n;

export default () => {
  const options = [
    {
      label: __('Desktop'),
      value: 'hide-on-xlarge',
      help: __('Hide this block on desktop'),
    },
    {
      label: __('Tablet'),
      value: 'hide-on-medium',
      help: __('Hide this block on medium'),
    },
    {
      label: __('Mobile'),
      value: 'hide-on-small',
      help: __('Hide this block on mobile devices'),
    },
  ];

  const addAttribute = settings => {
    if (settings.attributes !== undefined) {
      // eslint-disable-next-line no-param-reassign
      settings.attributes = Object.assign(settings.attributes, {
        hideon: {
          type: 'array',
          default: [],
        },
      });
    }

    return settings;
  };

  addFilter('blocks.registerBlockType', 'egap/attribute/hide-on', addAttribute);

  const withHideOn = createHigherOrderComponent(
    BlockEdit => props => {
      const { hideon = [] } = props.attributes;
      let { className = '' } = props.attributes;

      return (
        <Fragment>
          <BlockEdit {...props} />
          <InspectorControls>
            <PanelBody title={__('Hide on')} initialOpen={false}>
              {options.map(data => (
                <CheckboxControl
                  key={`${data.value}`}
                  label={data.label}
                  help={data.help}
                  checked={className.indexOf(`has-${data.value}`) > -1}
                  onChange={value => {
                    const newHideon = [...hideon];
                    className = className
                      .replace(`has-${data.value}`, '')
                      .trim()
                      .replace('/[ ]{2,}/g', ' ');

                    if (value) {
                      newHideon.push(data.value);
                      className += ` has-${data.value}`;
                    } else {
                      const index = newHideon.indexOf(data.value);
                      if (index > -1) {
                        newHideon.splice(index, 1);
                      }
                    }

                    props.setAttributes({ hideon: newHideon, className });
                  }}
                />
              ))}
            </PanelBody>
          </InspectorControls>
        </Fragment>
      );
    },
    'withHideOn',
  );

  addFilter('editor.BlockEdit', 'egap/with-hide-on', withHideOn);
};
