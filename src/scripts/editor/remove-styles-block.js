/* eslint-disable no-undef, no-unused-vars */

export default () => {
  wp.domReady(() => {
    wp.blocks.unregisterBlockStyle('core/separator', 'wide');
    wp.blocks.unregisterBlockStyle('core/separator', 'dots');
    wp.blocks.unregisterBlockType('core/buttons');
  });
};
